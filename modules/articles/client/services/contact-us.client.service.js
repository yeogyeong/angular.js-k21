(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.services')
    .factory('ContactUsService', ContactUsService);

  ContactUsService.$inject = ['$resource'];

  function ContactUsService($resource) {
    const service = $resource('/api/contact-us/:contactUsId', {
      contactUsId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      contactTotalCount: {
        method: 'GET',
        url: '/api/contactTotalCount'
      },
      contactGet: {
        method: 'GET',
        isArray: true
      }
    });


    angular.extend(service, {
      contactList
    });

    /**
     * Services for importing data
     */
    function contactList(page, searchOptions, searchKeyword) {
      return this.contactGet({
        page,
        searchOptions,
        searchKeyword
      }).$promise;
    }



    return service;

  }

}());
