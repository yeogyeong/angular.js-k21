(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.services')
    .factory('NoticeService', NoticeService);

NoticeService.$inject = ['$resource'];

function NoticeService($resource) {
  const service = $resource('/api/notice/:noticeId', {
    noticeId: '@_id'
  }, {
    noticeTotalCount: {
      method: 'GET',
      url: '/api/noticeTotalCount'
    },
    noticeGet: {
      method: 'GET',
      isArray: true
    },
    noticeTop: {
      method: 'GET',
      url: '/api/noticeTop'
    }
  });


  angular.extend(service, {
    noticeList
  });


  /**
   * Services for importing data
   */
  function noticeList(page, searchOptions, searchKeyword) {
    return this.noticeGet({
        page,
        searchOptions,
        searchKeyword
      })
      .$promise;
  }

  return service;

}

}());
