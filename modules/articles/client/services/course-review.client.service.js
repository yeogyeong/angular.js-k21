(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.services')
    .factory('CourseReviewService', CourseReviewService);

  /**
   * Dependency Injection
   */
  CourseReviewService.$inject = ['$resource'];

  /**
   * Course Review service for REST endpoint
   */
  function CourseReviewService($resource) {
    const service = $resource('/api/course-review/:courseReviewId', {
      courseReviewId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      courseReviewTotalCount: {
        method: 'GET',
        url: '/api/courseReviewTotalCount'
      },
      courseReviewGet: {
        method: 'GET',
        isArray: true
      },
      courseReviewTop: {
        method: 'GET',
        url: '/api/courseReviewTop'
      }
    });

    angular.extend(service, {
      courseReviewList
    });

    /**
     * Services for importing data
     */
    function courseReviewList(page, searchOptions, searchKeyword) {
      return this.courseReviewGet({
          page,
          searchOptions,
          searchKeyword
        })
        .$promise;
    }

    return service;
  }
}());
