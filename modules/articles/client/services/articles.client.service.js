(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.services')
    .factory('ArticleService', ArticleService);

  /**
   * Dependency Injection
   */
  ArticleService.$inject = ['$resource'];

  /**
   * FAQ service for REST endpoint
   */
  function ArticleService($resource) {
    return $resource('/api/articles', {}, {
      FAQ: {
        method: 'GET',
        url: '/api/faq/:faqId',
        isArray: 'ture'
      }
    });
  }
}());
