(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('articles', {
        abstract: true,
        url: '/articles',
        template: '<ui-view/>'
      })
      /**
       * articles : faq
       */
      .state('articles.faq', {
        url: '/faq',
        templateUrl: 'modules/articles/client/views/faq-articles.client.view.html',
        controller: 'FaqController',
        controllerAs: 'vm'
      })
      .state('articles.faq.view', {
        url: '/view/:faqCounter',
        templateUrl: 'modules/articles/client/views/faq-articles.client.view.html',
        controller: 'FaqController',
        controllerAs: 'vm'
      })

      /**
       * articles-event
       */
       .state('articles.event', {
         url: '/event',
         templateUrl: 'modules/articles/client/views/event/list-event.client.view.html'
       })
       .state('articles.event.view', {
         url: '/view',
         templateUrl: 'modules/articles/client/views/event/view-event.client.view.html'
       });

  }

}());
