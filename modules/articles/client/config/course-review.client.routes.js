(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('articles.course-review', {
        abstract: true,
        url: '/course-review',
        template: '<ui-view/>'
      })
      /**
       * course-review
       */
      .state('articles.course-review.read', {
        url: '/:courseReviewId/read',
        templateUrl: 'modules/articles/client/views/course-review/read-course-review.client.view.html',
        controller: 'ReadCourseReviewController',
        controllerAs: 'vm',
        resolve: {
          courseReviewResolve: getCourseReview
        }
      })
      .state('articles.course-review.list', {
        url: '/list?:page&:searchOptions&:searchKeyword',
        templateUrl: 'modules/articles/client/views/course-review/list-course-review.client.view.html',
        controller: 'ListCourseReviewController',
        controllerAs: 'vm'
      })
      .state('articles.course-review.edit', {
        url: '/:courseReviewId/edit',
        templateUrl: 'modules/articles/client/views/course-review/edit-course-review.client.view.html',
        controller: 'EditCourseReviewController',
        controllerAs: 'vm',
        resolve: {
          courseReviewResolve: getCourseReview
        }
      })
      .state('articles.course-review.write', {
        url: '/write',
        templateUrl: 'modules/articles/client/views/course-review/write-course-review.client.view.html',
        controller: 'WriteCourseReviewController',
        controllerAs: 'vm',
        resolve: {
          courseReviewResolve: newCourseReview
        }
      });
  }


/**
 * get Course Review
 */
  getCourseReview.$inject = ['$stateParams', 'CourseReviewService'];

  function getCourseReview($stateParams, CourseReviewService) {
    return CourseReviewService.get({
      courseReviewId: $stateParams.courseReviewId
    }).$promise;
  }

/**
 * new Course Review
 */
  newCourseReview.$inject = ['CourseReviewService'];

  function newCourseReview(CourseReviewService) {
    return new CourseReviewService();
  }




}());
