(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('articles.contact-us', {
        abstract: true,
        url: '/contact-us',
        template: '<ui-view/>'
      })
      /**
       * articles-contact-us
       */
      .state('articles.contact-us.list', {
        url: '/list?:page&:searchOptions&:searchKeyword',
        templateUrl: 'modules/articles/client/views/contact-us/list-contact-us.client.view.html',
        controller: 'ListContactUsController',
        controllerAs: 'vm'
      })
      .state('articles.contact-us.read', {
        url: '/:contactUsId/read',
        templateUrl: 'modules/articles/client/views/contact-us/read-contact-us.client.view.html',
        controller: 'ReadContactUsController',
        controllerAs: 'vm',
        resolve: {
          contactUsResolve: getContactUs
        }
      })
      .state('articles.contact-us.write', {
        url: '/write',
        templateUrl: 'modules/articles/client/views/contact-us/write-contact-us.client.view.html',
        controller: 'WriteContactUsController',
        controllerAs: 'vm',
        resolve: {
          contactUsResolve: newContactUs
        }
      })
      .state('articles.contact-us.edit', {
        url: '/:contactUsId/edit',
        templateUrl: 'modules/articles/client/views/contact-us/edit-contact-us.client.view.html',
        controller: 'EditContactUsController',
        controllerAs: 'vm',
        resolve: {
          contactUsResolve: getContactUs
        }
      });



    /**
     * Dependency Injection
     */
    getContactUs.$inject = ['$stateParams', 'ContactUsService'];

    /**
     * Get Contact Us resolve
     */
    function getContactUs($stateParams, ContactUsService) {
      /**
       * not signIn and reading secret messages,
       */
      // if (vm.contactUs.contactUsType && (vm.contactUs.user.username !== vm.auth.user.username)) {
      //     $window.alert('로그인 후 이용해주세요');
      //     $state.go('user.signin');
      //     return false;
      // }
      return ContactUsService.get({
        contactUsId: $stateParams.contactUsId
      }).$promise;
    }


    /**
     * new Contact Us
     */
    newContactUs.$inject = ['ContactUsService'];

    function newContactUs(ContactUsService) {
      return new ContactUsService();
    }



  }

}());
