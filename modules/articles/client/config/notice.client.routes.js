(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('articles.notice', {
        abstract: true,
        url: '/notice',
        template: '<ui-view/>'
      })
      /**
       * articles-notice
       */
      .state('articles.notice.list', {
        url: '/list?:page&:searchOptions&:searchKeyword',
        templateUrl: 'modules/articles/client/views/notice/list-notice.client.view.html',
        controller: 'ListNoticeController',
        controllerAs: 'vm'
      })
      .state('articles.notice.read', {
        url: '/:noticeId/read',
        templateUrl: 'modules/articles/client/views/notice/read-notice.client.view.html',
        controller: 'ReadNoticeController',
        controllerAs: 'vm',
        resolve: {
          noticeResolve: getNotice
        }
      });


    /**
     * get Notice
     */
    getNotice.$inject = ['$stateParams', 'NoticeService'];

    function getNotice($stateParams, NoticeService) {
      return NoticeService.get({
        noticeId: $stateParams.noticeId
      }).$promise;
    }


  }



}());
