(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .directive('faqSlide', faqSlide);

  /**
   * Dependency Injection
   */
  faqSlide.$inject = ['$state'];

  /**
   * Directive show Answer
   */
  function faqSlide($state) {
    const directive = {
      restrict: 'A',
      link
    };

    return directive;

    function link(scope, element, attrs) {

      const answerElement = element.children('dd');

      if (attrs.id === $state.params.faqCounter) {
        answerElement.addClass('show');
      }

      element.on('click', (e) => {
        angular.forEach(angular.element('dd'), (answer) => {
          angular.element(answer)
            .slideUp();
        });


        if (answerElement.css('display') !== 'block') {
          answerElement.slideDown({
            duration: 300,
            easing: 'linear'
          });
        }
      });

    }
  }
})();
