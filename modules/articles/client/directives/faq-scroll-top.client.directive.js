(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .directive('faqScrollTop', faqScrollTop);

  faqScrollTop.$inject = ['$timeout', '$state', '$window'];

  /**
   * Directive faq scroll
   */
  function faqScrollTop($timeout, $state, $window) {
    const directive = {
      restrict: 'A',
      link
    };

    return directive;

    function link(scope, element, attrs) {
      $timeout(function() {
        if ($state.params.faqCounter === attrs.id) {
          angular.element('html, body')
            .animate({
              scrollTop: angular.element(`#${$state.params.faqCounter}`).offset().top
            });
        }
      });

    }
  }
})();
