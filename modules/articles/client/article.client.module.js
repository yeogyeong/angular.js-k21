(function(app) {
  app.registerModule('articles', ['core']);
  app.registerModule('articles.services');
  app.registerModule('articles.routes', ['core.routes', 'articles.services']);
}(ApplicationConfiguration));
