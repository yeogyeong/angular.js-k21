(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles')
    .controller('ReadNoticeController', ReadNoticeController);

  /**
   * Dependency Injection
   */
  ReadNoticeController.$inject = ['noticeResolve', '$state'];

  function ReadNoticeController(notice, $state) {
    const vm = this;

    vm.notice = notice;
    vm.list = list;

    /**
     * Go to List Page using $state.previous.
     */
    function list() {
      if ($state.previous.params.page) {
        $state.go('articles.notice.list', {
          page: $state.previous.params.page
        });
      } else {
        $state.go('articles.notice.list', {
          page: 1
        });
      }
    }

    /**
     * textarea enter replace
     */
    vm.notice.contents = vm.notice.contents.replace(/(?:\r\n|\r|\n)/g, '<br />');
  }

}());
