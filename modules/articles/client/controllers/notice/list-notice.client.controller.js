(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles')
    .controller('ListNoticeController', ListNoticeController);

  /**
   * Dependency Injection
   */
  ListNoticeController.$inject = [
    '$window',
    '$state',
    'NoticeService'
  ];

  function ListNoticeController(
    $window,
    $state,
    NoticeService
  ) {
    const vm = this;

    vm.pageChanged = pageChanged;

    vm.searching = searching;
    vm.searchOptions = 'title';
    vm.search = {};


    /**
     * When there are no $state.params.page
     */
    if (!$state.params.page) {
      $state.go('articles.notice.list', {
        page: 1
      });
    }

    NoticeService.noticeTop((noticeTops) => {
      vm.noticeTops = noticeTops.noticeTop;
    });


    /**
     * To import Notice data and create a list
     */
    NoticeService.noticeList(
        $state.params.page,
        $state.params.searchOptions,
        $state.params.searchKeyword
      )
      .then((notices) => {
        vm.notices = notices;
        buildPager();
      })
      .catch((err) => {
        console.log(err);
      });

    /*
     * When you press the searching button
     * Using Parameters
     */
    function searching() {
      vm.currentPage = 1;
      vm.search.searchOptions = vm.searchOptions;

      if (!vm.searchKeyword) {
        vm.search.searchOptions = '';
      }

      $state.go('articles.notice.list', {
        page: vm.currentPage,
        searchOptions: vm.search.searchOptions,
        searchKeyword: vm.searchKeyword
      });
      buildPager();
    }

    /**
     * Import total number of data to create page count
     */
    function buildPager() {
      vm.search.searchOptions = $state.params.searchOptions;
      vm.search.searchKeyword = $state.params.searchKeyword;

      NoticeService.noticeTotalCount(vm.search)
        .$promise
        .then((totalCount) => {
          vm.totalItems = totalCount.counts;
          vm.currentPage = $state.params.page;
          vm.itemsPerPage = 10;
          vm.maxSize = 10;

          if (!$state.params.searchOptions) {
            vm.searchOptions = 'title';
          } else {
            vm.searchOptions = $state.params.searchOptions;
            vm.searchKeyword = $state.params.searchKeyword;
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    /**
     * Change page event
     */
    function pageChanged() {
      $state.go('articles.notice.list', {
        page: vm.currentPage
      });
    }


  }

}());
