(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.routes')
    .controller('FaqController', FaqController);

  /**
   * Dependency Injection
   */
  FaqController.$inject = [
    '$timeout',
    '$state',
    'ArticleService'
  ];

  /**
   * Configuring the FAQ list controller
   */
  function FaqController($timeout, $state, ArticleService) {

    const vm = this;

    ArticleService.FAQ((faqs) => {
      vm.faqs = faqs;
console.log(faqs);
      /**
      * textarea enter replace
      */
      // vm.faqs = vm.faqs.replace(/(?:\r\n|\r|\n)/g, '<br />');
    });

  }

}());
