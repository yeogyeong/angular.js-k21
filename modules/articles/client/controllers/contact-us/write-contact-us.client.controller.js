(function() {
  angular
    .module('articles.routes')
    .controller('WriteContactUsController', WriteContactUsController);


  WriteContactUsController.$inject = [
    '$window',
    '$state',
    'contactUsResolve',
    'Authentication'
  ];

  function WriteContactUsController(
    $window,
    $state,
    contactUs,
    Authentication
  ) {
    const vm = this;
    vm.contactUs = contactUs;
    vm.contactUs.contactUsType = 'public';

    vm.auth = Authentication;

    vm.write = write;

    function write() {

      vm.contactUs.providers = ['onestopedu'];
      vm.contactUs.user = vm.auth.user;

      if (!vm.contactUs.title) {
        $window.alert('제목을 입력해주세요');
        return false;
      } else if (!vm.contactUs.contents) {
        $window.alert('내용을 입력해주세요');
        return false;
      }

      vm.contactUs.$save()
        .then((response) => {
          $window.alert('글이 등록되었습니다');
          $state.go('articles.contact-us.list');
        })
        .catch((err) => {
          console.log(err);
        });
    }

  }
}());
