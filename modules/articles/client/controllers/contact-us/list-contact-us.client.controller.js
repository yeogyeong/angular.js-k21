(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.routes')
    .controller('ListContactUsController', ListContactUsController);

  /**
   * Dependency Injection
   */
  ListContactUsController.$inject = [
    '$window',
    '$state',
    'ContactUsService',
    'Authentication'
  ];

  /**
   * Configuring the Contact Us list controller
   */
  function ListContactUsController(
    $window,
    $state,
    ContactUsService,
    Authentication
  ) {

    const vm = this;

    vm.auth = Authentication;

    vm.searching = searching;
    vm.search = {};

    vm.write = write;
    vm.pageChanged = pageChanged;


    /**
     * When there are no $state.params.page
     */
    if (!$state.params.page) {
      $state.go('articles.contact-us.list', {
        page: 1
      });
    }


    /**
     * To import ContactUs data and create a list
     */
    ContactUsService.contactList(
        $state.params.page,
        $state.params.searchOptions,
        $state.params.searchKeyword
      )
      .then((contactUss) => {
        vm.contactUss = contactUss;
        buildPager();
      })
      .catch((err) => {
        console.log(err);
      });


    /*
     * When you press the searching button
     * Using Parameters
     */
    function searching() {
      vm.currentPage = 1;
      vm.search.searchOptions = vm.searchOptions;

      if (!vm.searchKeyword) {
        vm.search.searchOptions = '';
      }

      $state.go('articles.contact-us.list', {
        page: vm.currentPage,
        searchOptions: vm.search.searchOptions,
        searchKeyword: vm.searchKeyword
      });

      buildPager();
    }


    /**
     * Import total number of data to create page count
     */
    function buildPager() {
      vm.search.searchOptions = $state.params.searchOptions;
      vm.search.searchKeyword = $state.params.searchKeyword;

      ContactUsService.contactTotalCount(vm.search)
        .$promise
        .then((totalCount) => {
          vm.totalItems = totalCount.counts;
          vm.currentPage = $state.params.page;
          vm.itemsPerPage = 10;
          vm.maxSize = 10;

          if (!$state.params.searchOptions) {
            vm.searchOptions = 'title';
          } else {
            vm.searchOptions = $state.params.searchOptions;
            vm.searchKeyword = $state.params.searchKeyword;
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }


    /**
     * Change page event
     */
    function pageChanged() {
      $state.go('articles.contact-us.list', {
        page: vm.currentPage
      });
    }

    /*
     * When you press the writing button
     */
    function write() {
      /*
       * When you are not sign in
       */
      if (!vm.auth.user) {
        $window.alert('로그인이 필요한 서비스 입니다.');
        vm.auth.clickInPlace = 'contactUs';
        $state.go('user.signin');
        return false;
      }

      $state.go('articles.contact-us.write');
    }


  }



}());
