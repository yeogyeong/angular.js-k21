(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.routes')
    .controller('EditContactUsController', EditContactUsController);

  /**
   * Dependency Injection
   */
  EditContactUsController.$inject = [
    '$window',
    '$state',
    'contactUsResolve'
  ];

  /**
   * Course Review edit controller
   */
  function EditContactUsController(
    $window,
    $state,
    contactUs

  ) {

    const vm = this;

    vm.contactUs = contactUs;
    vm.editContactUs = editContactUs;


    /**
     * Check whether the button is private or public.
     */

    /**
     * edit contactUs($update)
     */
    function editContactUs(contactUsId) {
      vm.contactUs.$update()
        .then((response) => {
          $window.alert('글이 수정되었습니다');
          $state.go('articles.contact-us.read', {
            contactUsId
          });
        })
        .catch();
    }
  }

}());
