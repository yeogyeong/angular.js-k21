(function() {

  /**
   * Module Configuration
   */
  angular
    .module('articles.routes')
    .controller('ReadContactUsController', ReadContactUsController);

  /**
   * Dependency Injection
   */
  ReadContactUsController.$inject = [
    '$scope',
    '$window',
    '$state',
    'contactUsResolve',
    'Authentication'
  ];

  /**
   * Configuring the Contact Us read controller
   */
  function ReadContactUsController(
    $scope,
    $window,
    $state,
    contactUs,
    Authentication
  ) {

    const vm = this;

    vm.auth = Authentication;

    vm.contactUs = contactUs;
    vm.removeContactUs = removeContactUs;


    if (vm.contactUs.contactUsType === 'private' && !vm.auth.user.username) {
      $state.go('user.signin');
      return false;
    }

    /**
     * textarea enter replace
     */
    vm.contactUs.contents = vm.contactUs.contents.replace(/(?:\r\n|\r|\n)/g, '<br />');
    if (vm.contactUs.comments[0]) {
      vm.contactUs.comments[0].contents = vm.contactUs.comments[0].contents.replace(/(?:\r\n|\r|\n)/g, '<br />');
    }

    /**
     * remove ContactUs
     */
    function removeContactUs() {
      vm.contactUs.$remove()
        .then((response) => {
          $window.alert('글이 삭제되었습니다');
          $state.go('articles.contact-us.list');
        })
        .catch((err) => {
          console.log(err);
        });
    }

  }

}());
