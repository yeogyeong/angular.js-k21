(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .controller('ListCourseReviewController', ListCourseReviewController);

  /**
   * Dependency Injection
   */
  ListCourseReviewController.$inject = [
    '$window',
    '$state',
    'CourseReviewService',
    'Authentication'
  ];

  /**
   * List Course Review controller
   */
  function ListCourseReviewController(
    $window,
    $state,
    CourseReviewService,
    Authentication
  ) {

    const vm = this;
    vm.pageChanged = pageChanged;

    vm.writeCourseReview = writeCourseReview;

    vm.searching = searching;
    vm.search = {};



    /**
     * When there are no $state.params.page
     */
    if (!$state.params.page) {
      $state.go('articles.course-review.list', {
        page: 1
      });
    }

    /**
    * To import courseReviewTop data and create a list
    */
    CourseReviewService.courseReviewTop((courseReviewTops) => {
      vm.courseReviewTops = courseReviewTops.courseReviewsTop;
    });

    /**
     * To import courseReview data and create a list
     */
    CourseReviewService.courseReviewList(
        $state.params.page,
        $state.params.searchOptions,
        $state.params.searchKeyword
      )
      .then((courseReviews) => {
        vm.courseReviews = courseReviews;
        buildPager();
      })
      .catch((err) => {
        console.log(err);
      });


    /*
     * When you press the searching button
     * Using Parameters
     */
    function searching() {
      vm.currentPage = 1;
      vm.search.searchOptions = vm.searchOptions;

      if (!vm.searchKeyword) {
        vm.search.searchOptions = '';
      }

      $state.go('articles.course-review.list', {
        page: vm.currentPage,
        searchOptions: vm.search.searchOptions,
        searchKeyword: vm.searchKeyword
      });
      buildPager();

    }

    /**
     * Import total number of data to create page count
     */
    function buildPager() {
      vm.search.searchOptions = $state.params.searchOptions;
      vm.search.searchKeyword = $state.params.searchKeyword;
      CourseReviewService.courseReviewTotalCount(vm.search)
        .$promise
        .then((totalCount) => {
          vm.totalItems = totalCount.counts;
          vm.currentPage = $state.params.page;
          vm.itemsPerPage = 10;
          vm.maxSize = 10;

          if (!$state.params.searchOptions) {
            vm.searchOptions = 'title';
          } else {
            vm.searchOptions = $state.params.searchOptions;
            vm.searchKeyword = $state.params.searchKeyword;
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }


    /**
     * Change page event
     */
    function pageChanged() {
      $state.go('articles.course-review.list', {
        page: vm.currentPage
      });
    }


    /*
     * When you press the writing button
     */
    function writeCourseReview() {
      /*
       * When you are not sign in
       */
      if (!Authentication.user) {
        $window.alert('로그인이 필요한 서비스 입니다.');
        Authentication.clickInPlace = 'courseReview';
        $state.go('user.signin');
        return false;
      }

      $state.go('articles.course-review.write');
    }

  }


})();
