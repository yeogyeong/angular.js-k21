(function() {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .controller('ReadCourseReviewController', ReadCourseReviewController);

  /**
   * Dependency Injection
   */
  ReadCourseReviewController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'courseReviewResolve'
  ];

  /**
   * Read Course Review controller
   */
  function ReadCourseReviewController(
    $window,
    $state,
    Authentication,
    courseReviewResolve
  ) {

    const vm = this;
    vm.auth = Authentication;
    vm.courseReview = courseReviewResolve;
    /**
     * textarea enter replace
     */
    vm.courseReview.contents = vm.courseReview.contents.replace(/(?:\r\n|\r|\n)/g, '<br />');
    if (vm.courseReview.comments[0]) {
      vm.courseReview.comments[0].contents = vm.courseReview.comments[0].contents.replace(/(?:\r\n|\r|\n)/g, '<br />');
    }

    vm.remove = remove;

    /**
     * remove courseReview
     */
    function remove() {
      vm.courseReview.$remove()
        .then((response) => {
          $window.alert('글이 삭제되었습니다');
          $state.go('articles.course-review.list');
        })
        .catch((err) => {
          console.log(err);
        });
    }

  }

}());
