(function() {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .controller('EditCourseReviewController', EditCourseReviewController);

  /**
   * Dependency Injection
   */
  EditCourseReviewController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'courseReviewResolve'
  ];

  /**
   * Course Review edit controller
   */
  function EditCourseReviewController(
    $window,
    $state,
    Authentication,
    courseReviewResolve

  ) {

    const vm = this;

    vm.courseReview = courseReviewResolve;

    vm.edit = edit;

/**
 * edit courseReview($update)
 */
    function edit(courseReviewId) {
      vm.courseReview.$update()
        .then((response) => {
          $window.alert('수강후기가 수정되었습니다');
          $state.go('articles.course-review.read', {
            courseReviewId
          });
        })
        .catch();
    }
  }

}());
