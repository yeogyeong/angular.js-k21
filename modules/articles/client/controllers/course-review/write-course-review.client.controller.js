(function() {
  angular
    .module('core')
    .controller('WriteCourseReviewController', WriteCourseReviewController);


  WriteCourseReviewController.$inject = [
    '$window',
    '$state',
    '$q',
    '$timeout',
    'Authentication',
    'Upload',
    'courseReviewResolve',
    'MypageCourseService'
  ];

  function WriteCourseReviewController(
    $window,
    $state,
    $q,
    $timeout,
    Authentication,
    Upload,
    courseReviewResolve,
    MypageCourseService
  ) {
    const vm = this;
    vm.courseReview = courseReviewResolve;

    vm.auth = Authentication;

    vm.write = write;

    MypageCourseService.query((courses) => {
      vm.courses = courses;
      console.log(vm.courses);
      /**
       * Change to Korean on the productType
       */
      for (let courseIndex = 0; courseIndex < courses.length; courseIndex += 1) {
        if (courses[courseIndex].productType === 'telephone') {
          courses[courseIndex].productType = '전화';
        } else if (courses[courseIndex].productType === 'skype') {
          courses[courseIndex].productType = '스카이프';
        } else if (courses[courseIndex].productType === 'screenBoard') {
          courses[courseIndex].productType = '화상칠판';
        }
      }
    });


    function write() {
      vm.courseReview.user = vm.auth.user;
      vm.courseReview.providers = ['onestopedu'];

      if (!vm.courseReview.courseList) {
        $window.alert('수강목록을 선택해 주세요.\n수강목록이 없는 경우 수강후기를 작성할 수 없습니다.');
        return false;
      } else if (!vm.courseReview.title) {
        $window.alert('제목을 입력해주세요');
        return false;
      } else if (!vm.courseReview.contents) {
        $window.alert('내용을 입력해주세요');
        return false;
      }

      fileUploads(vm.courseReview)
        .then(saveCourseReview)
        .then((courseReview) => {
          $state.go('articles.course-review.read', {
            courseReviewId: courseReview._id
          });
        })
        .catch((err) => {
          console.log(err);
        });

    }

    /**
     * Save courseReview promise
     */
    function saveCourseReview(courseReview) {
      const deferred = $q.defer();

      vm.courseReview.$save(onSaveReviewSuccess, onSaveReviewError);

      return deferred.promise;

      // Coruse-Review save success callback
      function onSaveReviewSuccess(courseReview) {
        deferred.resolve(courseReview);
        $window.alert('수강후기가 등록되었습니다');
        $state.go('articles.course-review.list');
      }

      // Coruse-Review save error callback
      function onSaveReviewError(err) {
        deferred.reject(err);
        console.log(err);
      }
    }


    /**
     * Upload file
     */
    function fileUploads(courseReview) {
      const deferred = $q.defer();

      if (vm.attachmentFiles && vm.attachmentFiles.length > 0) {

        vm.attachmentFiles.forEach((fileItem, index, array) => {
          console.log(fileItem);
          Upload.upload({
            url: 'http://1-stopedu.com:3000/api/file-item/upload',
            method: 'POST',
            data: {
              newFileItem: fileItem,
              filterName: 'attachFileFilter'
            }
          });
        });

      } else {
        deferred.resolve(courseReview);
      }

      return deferred.promise;


    }


  }
}());
