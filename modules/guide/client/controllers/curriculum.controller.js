(function() {

  /**
   * Module Configuration
   */
  angular
    .module('guide.routes')
    .controller('CurriculumController', CurriculumController);

  /**
   * Dependency Injection
   */
  CurriculumController.$inject = [
    '$scope',
    '$state',
    '$anchorScroll',
    '$location'
  ];

  /**
   * Configuring the Curriculum Controller
   */
  function CurriculumController(
    $scope,
    $state,
    $anchorScroll,
    $location
  ) {

    const vm = this;

    vm.curriculum = curriculum;
    function curriculum(id) {
      $state.go('guide.curriculum.regular').then(() => {
        $location.hash(id);
        $anchorScroll();
      });
    }

    vm.junior = junior;
    function junior(id) {
      $state.go('guide.curriculum.junior').then(() => {
        $location.hash(id);
        $anchorScroll();
      });
    }

    vm.freetalking = freetalking;
    function freetalking(id) {
      $state.go('guide.curriculum.freetalking').then(() => {
        $location.hash(id);
        $anchorScroll();
      });
    }

    vm.business = business;
    function business(id) {
      $state.go('guide.curriculum.business').then(() => {
        $location.hash(id);
        $anchorScroll();
      });
    }

    vm.interview = interview;
    function interview(id) {
      $state.go('guide.curriculum.interview').then(() => {
        $location.hash(id);
        $anchorScroll();
      });
    }

    vm.exam = exam;

    function exam(id) {
      $state.go('guide.curriculum.exam').then(() => {
        $location.hash(id);
        $anchorScroll();
      });
    }


  }

}());
