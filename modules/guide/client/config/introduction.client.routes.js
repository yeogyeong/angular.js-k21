(function() {

  /**
   * Module Configuration
   */
  angular
    .module('guide')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('guide.introduction', {
        abstract: true,
        url: '/introduction',
        template: '<ui-view/>'
      })
      /**
       * introduction company
       */
      .state('guide.introduction.company', {
        url: '/company',
        templateUrl: 'modules/guide/client/views/introduction/company-introduction.html'
      })
      .state('guide.introduction.tutor-system', {
        url: '/tutor-system',
        templateUrl: 'modules/guide/client/views/introduction/tutor-system-introduction.client.view.html'
      })
      /**
       * introduction tutor
       */
      .state('guide.introduction.tutor', {
        abstract: true,
        url: '/tutor',
        template: '<ui-view/>'
      })
      .state('guide.introduction.tutor.eu', {
        url: '/eu',
        templateUrl: 'modules/guide/client/views/introduction/tutor-introduction/eu-tutor-introduction.client.view.html'
      })
      .state('guide.introduction.tutor.ph', {
        url: '/ph',
        templateUrl: 'modules/guide/client/views/introduction/tutor-introduction/ph-tutor-introduction.client.view.html'
      })
      .state('guide.introduction.tutor.us', {
        url: '/us',
        templateUrl: 'modules/guide/client/views/introduction/tutor-introduction/us-tutor-introduction.client.view.html'
      });
  }

}());
