(function() {

  /**
   * Module Configuration
   */
  angular
    .module('guide')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('guide.curriculum', {
        abstract: true,
        url: '/curriculum',
        template: '<ui-view/>'
      })
      /**
       * curriculum
       */
       .state('guide.curriculum.total', {
         url: '/total',
         templateUrl: 'modules/guide/client/views/curriculum/total-process-curriculum.client.view.html',
         controller: 'CurriculumController',
         controllerAs: 'vm'
       })
       .state('guide.curriculum.regular', {
         url: '/regular',
         templateUrl: 'modules/guide/client/views/curriculum/regular-conversation-curriculum.client.view.html'
       })
       .state('guide.curriculum.junior', {
         url: '/junior',
         templateUrl: 'modules/guide/client/views/curriculum/junior-conversation-curriculum.client.view.html'
       })
       .state('guide.curriculum.freetalking', {
         url: '/freetalking',
         templateUrl: 'modules/guide/client/views/curriculum/freetalking-curriculum.client.view.html'
       })
      .state('guide.curriculum.business', {
        url: '/business',
        templateUrl: 'modules/guide/client/views/curriculum/business-curriculum.client.view.html'
      })
      .state('guide.curriculum.interview', {
        url: '/interview',
        templateUrl: 'modules/guide/client/views/curriculum/interview-curriculum.client.view.html'
      })
      .state('guide.curriculum.exam', {
        url: '/exam',
        templateUrl: 'modules/guide/client/views/curriculum/exam-preparation-curriculum.client.view.html'
      });
  }

}());
