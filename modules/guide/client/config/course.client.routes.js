(function() {

  /**
   * Module Configuration
   */
  angular
    .module('guide')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('guide.course', {
        abstract: true,
        url: '/course',
        template: '<ui-view/>'
      })
      /**
       * course
       */
      .state('guide.course.overseas', {
        url: '/overseas',
        templateUrl: 'modules/guide/client/views/course/overseas-course.client.view.html'
      })
      .state('guide.course.process', {
        url: '/process',
        templateUrl: 'modules/guide/client/views/course/process-course.client.view.html'
      })
      .state('guide.course.regulation', {
        url: '/regulation',
        templateUrl: 'modules/guide/client/views/course/regulation-course.client.view.html'
      });

  }

}());
