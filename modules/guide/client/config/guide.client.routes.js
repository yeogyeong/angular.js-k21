(function() {

  /**
   * Module Configuration
   */
  angular
    .module('guide.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('guide', {
        abstract: true,
        url: '/guide',
        template: '<ui-view/>'
      })
      .state('guide.leveltest', {
        url: '/leveltest',
        templateUrl: 'modules/guide/client/views/leveltest/guide-leveltest.client.view.html'
      })
      /**
       * program Support
       */
      .state('guide.program', {
        abstract: true,
        url: '/program',
        template: '<ui-view/>'
      })
      .state('guide.program.internet', {
        url: '/internet',
        templateUrl: 'modules/guide/client/views/program/internet-program.client.view.html'
      })
      .state('guide.program.pc', {
        url: '/pc',
        templateUrl: 'modules/guide/client/views/program/pc-program.client.view.html'
      })
      .state('guide.program.recording', {
        url: '/recording',
        templateUrl: 'modules/guide/client/views/program/recording-program.client.view.html'
      })
      .state('guide.program.self', {
        url: '/self',
        templateUrl: 'modules/guide/client/views/program/self-program.client.view.html'
      });
  }

}());
