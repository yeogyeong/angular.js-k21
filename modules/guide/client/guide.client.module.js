(function(app) {
  app.registerModule('guide', ['core']);
  app.registerModule('guide.services');
  app.registerModule('guide.routes', ['core.routes', 'guide.services']);
}(ApplicationConfiguration));
