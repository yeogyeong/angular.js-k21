(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register')
    .directive('desireTimer', desireTimer);

  desireTimer.$inject = ['desireTimerConfig'];

  /**
   * Directive apply for leveltest desire datetime
   */
  function desireTimer(desireTimerConfig) {
    // console.log(desireTimerConfig);
    const directive = {
      restrict: 'A',
      templateUrl: './modules/register/client/templates/desire-timer.client.view.html',
      require: ['^desireTimer', '^ngModel'],
      scope: {
        startHour: '@start',
        endHour: '@end'
      },
      controller,
      controllerAs: 'ctrl',
      link
    };

    return directive;


    /**
     * Link
     */
    function link(scope, elem, attrs, ctrls) {
      // console.log(ctrls);
      const ctrl = ctrls[0];
      const ngModel = ctrls[1];

      ctrl.init(ngModel);
    }

    /**
     * Controller
     */
    function controller($scope, $element) {
      const ctrl = this;
// console.log($element);
      const startHour = angular.isString($scope.startHour) ?
        parseInt($scope.startHour, 10) : desireTimerConfig.defaultStartHour;
      const endHour = angular.isString($scope.endHour) ?
        parseInt($scope.endHour, 10) : desireTimerConfig.defaultEndHour;

      ctrl.init = function(ngModel) {
        ctrl.ngModel = ngModel;
        ctrl.desireDate = ctrl.initDesireDate();
      };

      ctrl.select = function(dateset) {
        // console.log(dateset);
        if (!dateset.isBefore) {
          ctrl.active = dateset.id;

          // "vm.leveltest.desired" model binding. ex) 2019-11-04 16:20:00
          const desired = moment(ctrl.desireDate || {})
            .hour(dateset.hour)
            .minute(dateset.minute)
            .second(0);
          // console.log(desired);
          ctrl.ngModel.$setViewValue(desired);
        }

      };

      ctrl.createRows = function(startHour, endHour) {
        const row = [];
        for (let hour = startHour; hour < endHour; hour += 1) {
          row.push({
            title: hour,
            datesets: ctrl.createDatesets(hour)
          });
        }
        return row;
      };

      ctrl.createDatesets = function(hour) {
        const dateset = [];
        for (let minute = 0; minute < 50; minute += 10) {

          dateset.push({
            hour,
            minute,
            // ex) 14:20
            label: ((hour.toString().length > 1) ? hour : '0' + hour) + ':' + ((minute.toString().length > 1) ? minute : '0' + minute),
            // ex) ht-620
            id: 'ht-' + hour + minute,
            // Check if a moment is before another moment.
            // The first argument will be parsed as a moment, if not already so.
            isBefore: moment(ctrl.desireDate || {})
              .hour(hour)
              .minute(minute)
              .second(0)
              .isBefore(moment())
          });
        }
        // console.log(dateset);
        return dateset;
      };

      ctrl.initDesireDate = function(date) {
        // console.log(date);
        return angular.isDefined(date) ?
          moment(date).hour(0).minute(0).second(0) :
          moment().hour(0).minute(0).second(0);
      };

      ctrl.rows = ctrl.createRows(startHour, endHour);

      $scope.$on('ui:datepicker.desiredate', (e, desireDate) => {
        // console.log(e, desireDate);
        ctrl.ngModel.$setViewValue(undefined);
        ctrl.active = undefined;
        ctrl.desireDate = ctrl.initDesireDate(desireDate);
        ctrl.rows = ctrl.createRows(startHour, endHour);
      });
    }
  }


}());
