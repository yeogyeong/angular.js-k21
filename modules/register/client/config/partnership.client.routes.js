(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('register', {
        abstract: true,
        url: '/register',
        template: '<ui-view/>'
      })
      /**
       * partnership
       */
      .state('register.partner', {
        url: '/partner',
        templateUrl: 'modules/register/client/views/partner-support.client.view.html',
        controller: 'partnerController',
        controllerAs: 'vm'
      });

  }

}());
