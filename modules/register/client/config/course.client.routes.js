(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('register.course', {
        abstract: true,
        url: '/course',
        template: '<ui-view/>'
      })
      /**
       * course
       */
       .state('register.course.apply', {
         url: '/apply',
         templateUrl: 'modules/register/client/views/course/apply-course.client.view.html',
         controller: 'ApplyCourseController',
         controllerAs: 'vm'
       })
       .state('register.course.apply-confirm', {
         url: '/apply-confirm',
         templateUrl: 'modules/register/client/views/course/apply-confirm-course.client.view.html',
         controller: 'ApplyCourseConfirmController',
         controllerAs: 'vm',
         params: {
           products: null,
           payments: null
         }
       });
  }

}());
