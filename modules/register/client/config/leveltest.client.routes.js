(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('register.leveltest', {
        abstract: true,
        url: '/leveltest',
        template: '<ui-view/>'
      })
      /**
       * leveltest
       */
      .state('register.leveltest.apply', {
        url: '/apply',
        templateUrl: 'modules/register/client/views/leveltest/apply-leveltest.client.view.html',
        controller: 'ApplyLeveltestController',
        controllerAs: 'vm',
        resolve: {
          guestResolve: newGuest
        }
      })
      .state('register.leveltest.apply-confirm', {
        url: '/apply-confirm',
        templateUrl: 'modules/register/client/views/leveltest/apply-confirm-leveltest.client.view.html',
        controller: 'ApplyConfirmController',
        controllerAs: 'vm',
        params: {
          apply: null
        }
      });


      /**
       * Dependency Injection
       */
      newGuest.$inject = ['Guest'];

      /**
       * New guest resolve
       */
      function newGuest(Guest) {
        return new Guest();
      }

  }

}());
