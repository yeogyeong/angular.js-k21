(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.services')
    .factory('PartnerService', PartnerService);

  /**
   * Dependency Injection
   */
  PartnerService.$inject = ['$resource'];

  /**
   * partership service for REST endpoint
   */
  function PartnerService($resource) {
    return $resource('/api/resource/partnership', {}, {
      partnership: {
        method: 'POST'
      }
    });
  }
}());
