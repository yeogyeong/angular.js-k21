(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.services')
    .factory('CoursePaymentService', CoursePaymentService);

  /**
   * Dependency Injection
   */
  CoursePaymentService.$inject = ['$resource'];

  /**
   * Course Review service for REST endpoint
   */
  function CoursePaymentService($resource) {
    return $resource('/api/payment/:paymentId', {
      paymentId: '@_id'
    }, {
      payment: {
        url: '/api/payment',
        method: 'POST'
      }
    });

  }
}());
