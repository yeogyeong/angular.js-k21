(function() {
  /**
   * Module Configuration
   */
  angular
    .module('register.services')
    .factory('Leveltest', Leveltest);

  /**
   * Dependency Injection
   */
  Leveltest.$inject = ['$resource'];

  function Leveltest($resource) {
    const service = $resource('/api/leveltest/:leveltestId', {
      leveltestId: '@_id'
    }, {
      apply: {
        method: 'POST',
        url: '/api/leveltest'
      }
    });

    return service;
  }
}());
