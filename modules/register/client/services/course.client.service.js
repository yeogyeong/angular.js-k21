(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.services')
    .factory('CourseService', CourseService);

  /**
   * Dependency Injection
   */
  CourseService.$inject = ['$resource'];

  /**
   * Course Review service for REST endpoint
   */
  function CourseService($resource) {
    const service = $resource('/api/course/:courseId', {
      courseId: '@_id'
    }, {
      getCourse: {
        method: 'GET',
        isArray: true
      },
      product: {
        url: '/api/product',
        method: 'POST'
      }
    });

    angular.extend(service, {
      courseList,
      productAmount
    });

    /**
     * Services for importing data
     */
    function courseList(courseId) {
      return this.getCourse({
        courseId
      }).$promise;
    }

    function productAmount(products) {
      return this.product(products).$promise;
    }



    return service;
  }
}());
