(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register')
    .constant('desireTimerConfig', {
      defaultStartHour: 6,
      defaultEndHour: 24
    });
}());
