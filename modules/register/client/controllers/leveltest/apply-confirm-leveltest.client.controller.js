(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.routes')
    .controller('ApplyConfirmController', ApplyConfirmController);

  /**
   * Dependency Injection
   */
  ApplyConfirmController.$inject = [
    '$state',
    'Leveltest'
  ];

  /**
   * Configuring the Apply Leveltest controller
   */
  function ApplyConfirmController(
    $state,
    Leveltest
  ) {
    const vm = this;

    vm.apply = new Leveltest($state.params.apply);

    if (!$state.params.apply || $state.params.apply === null) {
      $state.go('register.leveltest.apply');
    }


  }

}());
