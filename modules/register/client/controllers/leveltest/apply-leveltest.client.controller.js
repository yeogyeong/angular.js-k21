(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.routes')
    .controller('ApplyLeveltestController', ApplyLeveltestController);

  /**
   * Dependency Injection
   */
  ApplyLeveltestController.$inject = [
    '$scope',
    '$window',
    '$state',
    'Leveltest',
    'Authentication',
    'Guest'
  ];

  /**
   * Configuring the Apply Leveltest controller
   */
  function ApplyLeveltestController(
    $scope,
    $window,
    $state,
    Leveltest,
    Authentication,
    Guest
  ) {
    const vm = this;
    vm.auth = Authentication;
    vm.apply = apply;
    vm.application = {};
    vm.leveltest = {};
    vm.guest = {};

    vm.choiceUrl = choiceUrl;
    vm.popTerms = popTerms;
    vm.mobileRex = validateMobile();


    /**
     * Importing data when logged in
     */
    if (vm.auth.user) {
      vm.application.koreanName = vm.auth.user.koreanName;
      vm.application.englishName = vm.auth.user.englishName;
      vm.application.mobile = vm.auth.user.mobile;
      const emails = vm.auth.user.email.split('@');
      vm.emailId = emails[0];
      vm.emailUrl = emails[1];
      vm.application.skype = vm.auth.user.skype;
    }


    /*
     * Click on screenBoard when not logged in.
     */
    $scope.$watch('vm.application.productType', (type) => {
      if (type === 'screenBoard' && !vm.auth.user) {
        if ($window.confirm('화상칠판 선택시 로그인이 필요합니다. 로그인하시겠습니까?')) {
          vm.application.productType = '';
          $state.go('user.signin', {
            addr: 'leveltest.apply'
          });
        } else {
          vm.application.productType = '';
        }
      }
    });


    /*
     * Automatic email input
     */
    function choiceUrl() {
      vm.emailUrl = vm.choiceEmail;
    }

    /*
     * show termPersonal
     */
    function popTerms() {
      vm.isterms = true;
    }

    /**
     * Validation function for mobile
     */
    function validateMobile() {
      return new RegExp(/^[0-9]*$/);
    }


    function apply(isValid) {
      vm.application.email = vm.emailId + '@' + vm.emailUrl;
      const emailRex = new RegExp(/^(\w+\.)*\w+@(\w+\.)+[A-Za-z]+/);



      if (isValid && vm.termPersonal) {
        successLevelTest();
      } else {
        errorLevelTest();
      }


      function errorLevelTest() {
        /**
         * Validate form
         */
        if (!isValid) {
          $scope.$broadcast('show-errors-check-validity', 'vm.form.leveltestForm');
        }


        if (!vm.application.koreanName) {
          $window.alert('이름을 입력해주세요');
        } else if (!vm.application.mobile) {
          $window.alert('전화번호를 입력해주세요');
        } else if (vm.application.mobile.length < 9) {
          $window.alert('정확한 번호를 입력해주세요');
        } else if (!vm.emailId || !vm.emailUrl) {
          $window.alert('이메일을 입력해주세요');
        } else if (!emailRex.test(vm.application.email)) {
          $window.alert('정확한 이메일주소를 입력해주세요');
        } else if (!vm.date) {
          $window.alert('수업날짜를 선택해주세요');
        } else if (!vm.application.skill) {
          $window.alert('회화수준을 선택해주세요');
        } else if (!vm.application.productType) {
          $window.alert('수업방식을 선택해주세요');
        } else if (vm.application.productType === 'skype' && !vm.application.skype) {
          $window.alert('스카이프ID를 입력해주세요');
        } else if (!vm.application.desired) {
          $window.alert('수업시간을 선택해주세요');
        } else if (!vm.termPersonal) {
          $window.alert('개인정보 취급 방침에 동의해주세요');
        }

        return false;
      }


      function successLevelTest() {
        if (!vm.auth.user) {
          vm.guest = vm.application;
          Guest.signup(vm.guest)
            .$promise
            .then(saveLeveltest)
            .catch(saveError);
        } else {
          saveLeveltest(vm.auth.user);
        }
      }


      /**
       * Save Leveltest
       */
      function saveLeveltest(user) {
        /**
         * When sending data to Apply Confirm Page
         */
        vm.application.desired = vm.application.desired.format('YYYY-MM-DD HH:mm');
        vm.leveltest.application = vm.application;
        vm.leveltest.user = user._id;
        
        Leveltest.apply(vm.leveltest)
          .$promise
          .then((res) => {
            if (!res._id) {
              $window.alert('무료 체험 신청에 실패하셨습니다');
              return false;
            }

            $state.go('register.leveltest.apply-confirm', {
              apply: vm.leveltest.application
            });
          })
          .catch((error) => {
            console.log(error);
          });

      }

      /**
       * Error callback
       */
      function saveError(err) {
        console.log(err);
      }


    }



  }

}());
