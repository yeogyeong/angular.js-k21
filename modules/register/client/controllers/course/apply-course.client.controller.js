(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.routes')
    .controller('ApplyCourseController', ApplyCourseController);

  /**
   * Dependency Injection
   */
  ApplyCourseController.$inject = [
    '$scope',
    '$window',
    '$state',
    '$http',
    'Authentication',
    'CourseService',
    'CouponService',
    'CoursePaymentService'
  ];

  /**
   * Configuring the Apply Course controller
   */
  function ApplyCourseController(
    $scope,
    $window,
    $state,
    $http,
    Authentication,
    CourseService,
    CouponService,
    CoursePaymentService
  ) {
    const vm = this;
    vm.auth = Authentication.user;

    vm.product = {};
    vm.product.curriculum = 'conversation';
    vm.hopeTime = '06';
    vm.hopeMinutes = '00';

    vm.useAllPoint = useAllPoint;
    vm.availablePoint = vm.auth.point;

    vm.couponType = couponType;

    vm.payment = {};
    vm.payment.method = 'CC';

    vm.applyCourse = applyCourse;

    if (vm.auth) {
      vm.coupons = CouponService.query();
    }

    $scope.$watch('vm.product.productType', (productType) => {
      const exceptTelephone = (productType === 'screenBoard' || productType === 'skype');
      const notAvailableTime = (vm.lessonTimes === '2/10' || vm.lessonTimes === '3/10' || vm.lessonTimes === '2/10');
      if (exceptTelephone && notAvailableTime) {
        vm.lessonTimes = '';
      }
      coursePrice();
    });

    $scope.$watch('vm.product.nation', (nation) => {
      coursePrice();
    });

    $scope.$watch('vm.product.month', (month) => {
      coursePrice();
    });

    $scope.$watch('vm.lessonTimes', (lessonTimes) => {
      if (lessonTimes) {
        const lessonTimesArray = lessonTimes.split('/');
        vm.product.times = lessonTimesArray[0];
        vm.product.minutes = lessonTimesArray[1];
        coursePrice();
      }
    });


    vm.time = [];
    for (let i = 6; i < 24; i += 1) {
      vm.num = i;
      if (i < 10) {
        vm.num = '0' + i;
      }
      vm.time.push(vm.num);
    }



    function coursePrice() {
      if (vm.product.productType && vm.product.nation && vm.product.month && vm.lessonTimes) {
        CourseService.productAmount(vm.product)
          .then((payments) => {
            vm.payments = payments;
            // console.log(vm.payments);
            vm.payment.totalPrice = vm.payments.price;
            vm.payment.point = 0;
            vm.payment.coupon = '';
          })
          .catch((err) => {
            console.log(err);
          });
      }
    }


    function useAllPoint() {
      if (!vm.payment.totalPrice) {
        $window.alert('먼저 상품을 선택해주세요. 금액이 설정되지 않았습니다.');
        return false;
      } else {
        vm.payment.point = vm.auth.point;
      }
    }


    $scope.$watch('vm.payment.point', (usePoint, previousPoint) => {
      usePoint = Number(usePoint);
      if (usePoint) {
        if (!vm.payment.totalPrice) {
          $window.alert('먼저 상품을 선택해주세요. 금액이 설정되지 않았습니다.');
          vm.payment.point = 0;
          return false;
        } else if (usePoint > vm.auth.point) {
          $window.alert('현재 보유한 포인트보다 높은 금액은 입력할 수 없습니다.');
          vm.payment.point = 0;
          return false;
        } else {
          vm.availablePoint = vm.auth.point - usePoint;
          vm.payment.totalPrice -= usePoint;
          vm.payment.totalPrice += Number(previousPoint);
        }
      } else if ((usePoint === 0 || !usePoint) && vm.payment.totalPrice) {
        vm.availablePoint = vm.auth.point;
        vm.payment.totalPrice = vm.payments.price;
        if (vm.payment.coupon) {
          couponType();
        }
      }
    });


    function couponType() {

      if (!vm.payment.totalPrice) {
        $window.alert('먼저 상품을 선택해주세요. 금액이 설정되지 않았습니다.');
        vm.payment.coupon = '';
        return false;
      } else {
        const pointUsedPrice = (vm.payments.price - vm.payment.point);

        if (!vm.payment.coupon) {
          vm.payment.totalPrice = (vm.payment.point) ? pointUsedPrice : vm.payments.price;
        } else {
          vm.coupons.forEach((item) => {

            if (item._id === vm.payment.coupon) {
              const savePrice = (vm.payments.price / item.amount);
              const isRate = (item.discountType === 'rate');

              if (vm.payment.point) {
                vm.payment.totalPrice = isRate ? (pointUsedPrice - savePrice) : (pointUsedPrice - item.amount);
              } else {
                vm.payment.totalPrice = isRate ? (vm.payments.price - savePrice) : (vm.payments.price - item.amount);
              }
            }

          });
        }

      }

    }


    function applyCourse() {
      /**
       * When you are not signIn
       */
      if (!vm.auth) {
        if ($window.confirm('로그인이 필요한 서비스 입니다 \n회원가입을 하시겠습니까?')) {
          $state.go('user.signin');
        }
        return false;
      } else if (!(vm.product.productType && vm.product.nation && vm.product.month && vm.lessonTimes)) {
        $window.alert('상품을 선택하지 않으셨거나, \n선택하신 상품 과정이 준비되어 있지 않습니다.');
        return false;
      } else if (!vm.hopeDay) {
        $window.alert('수업 시작일을 선택해주세요');
        return false;
      } else {
        vm.productId = vm.payments._id;

        vm.payment.desired = vm.hopeDay + ' ' + vm.hopeTime + ':' + vm.hopeMinutes + ':00';

        if (vm.payment.method === 'CC') {
          $window.alert('다른 결제 수단 이용');
          return false;
        } else {
          CoursePaymentService.payment({
              product: vm.productId,
              payment: vm.payment
            })
            .$promise
            .then((res) => {
              console.log(res);
            })
            .catch((err) => {
              console.log(err);
            });
        }
        console.log({
          product: vm.productId,
          payment: vm.payment
        });


        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8081/proxy/lg_xpay/payreq_crossplatform.php',
        //     data: applyInfo,
        //     headers: {
        //       'Content-Type': 'application/json; charset=utf-8'
        //     }
        //   })
        //   .then((res) => {
        //     console.log(res);
        //   })
        //   .catch((err) => {
        //     console.log('서버와연결 오류', err);
        //   });


        // $state.go('register.course.apply-confirm', {
        //   products: vm.product,
        //   payments: vm.payment
        // });
      }
    }


  }

}());
