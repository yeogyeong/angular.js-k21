(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.routes')
    .controller('ApplyCourseConfirmController', ApplyCourseConfirmController);

  /**
   * Dependency Injection
   */
  ApplyCourseConfirmController.$inject = [
    '$scope',
    '$window',
    '$state',
    'Authentication',
    'CourseService'
  ];

  /**
   * Configuring the Apply Course controller
   */
  function ApplyCourseConfirmController(
    $scope,
    $window,
    $state,
    Authentication,
    CourseService
  ) {
    const vm = this;
    vm.auth = Authentication.user;

    // vm.products = new CourseService($state.params.products);
    // vm.payments = new CourseService($state.params.payments);
    // console.log('vm.products: ', vm.products);
    // console.log('vm.payments: ', vm.payments);


  }

}());
