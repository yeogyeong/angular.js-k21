(function() {

  /**
   * Module Configuration
   */
  angular
    .module('register.routes')
    .controller('partnerController', partnerController);

  /**
   * Dependency Injection
   */
  partnerController.$inject = [
    '$scope',
    '$window',
    '$state',
    'PartnerService'
  ];

  /**
   * Configuring the partner controller
   */
  function partnerController(
    $scope,
    $window,
    $state,
    PartnerService
  ) {

    const vm = this;
    vm.partner = {};

    vm.emailUrl = emailUrl;
    vm.partnership = partnership;

    vm.numberValid = numberValid;
    vm.isPartnerClick = false;



    /**
     * When the phone number is not a number
     */
    function numberValid() {
      const mobileValid = /^[0-9-]*$/;

      if (!mobileValid.test(vm.partner.mobile)) {
        vm.partner.mobile = vm.partner.mobile.replace(/[^0-9-]/g, '');
        return false;
      }
    }

    function partnership(isValid) {
      vm.isPartnerClick = true;
      /**
       * Validate form
       */
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.partnerForm');
        return false;
      } else if (!vm.partner.contents) {
        $window.alert('문의내용을 입력해주세요');
        return false;
      } else if (!vm.termPersonal) {
        $window.alert('개인정보 취급 방침 동의가 필요합니다.');
        return false;
      }


      vm.partner.email = `${vm.relationerEmailId}@${vm.relationerEmailUrl}`;
      vm.partner.category = 'partnership';
      vm.partner.provider = 'onestopedu';

      PartnerService.partnership(vm.partner)
        .$promise
        .then((res) => {
          $window.alert('요청하신 제휴 문의가 성공적으로 접수되었습니다');
          $window.location.reload();
        })
        .catch((err) => {
          console.log('err', err);
        });
    }

    /*
     * Automatic email input
     */
    function emailUrl() {
      vm.relationerEmailUrl = vm.choiceEmail;
    }

  }
}());
