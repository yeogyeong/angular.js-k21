(function(app) {
  app.registerModule('register', ['core']);
  app.registerModule('register.services');
  app.registerModule('register.routes', ['core.routes', 'register.services']);
}(ApplicationConfiguration));
