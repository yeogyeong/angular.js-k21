(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .factory('AuthInterceptorService', AuthInterceptorService);

  /**
   * Dependency Injection
   */
  AuthInterceptorService.$inject = ['$q', '$state', 'Authentication'];

  /**
   * Configuring authentication interceptor service
   */
  function AuthInterceptorService($q, $state, Authentication) {
    const service = {
      responseError
    };

    return service;

    function responseError(rejection) {
      if (!rejection.config.ignoreAuthModule) {
        switch (rejection.status) {
          case 400:
            {
              $state.go('bad-request', {
                message: rejection.data.message
              });
              break;
            }
          case 401:
            {
              Authentication.user = null;
              $state.transitionTo('authentication.signin');
              break;
            }
          case 403:
            {
              $state.transitionTo('forbidden');
              break;
            }
          case 404:
            {
              $state.go('not-found', {
                message: rejection.data.message
              });
              break;
            }
          case -1:
            {

              /**
               * Handle error if no response from server(Network Lost or Server not responding)
               */
              // const toastr = $injector.get('toastr');
              // toastr.error('No response received from server. Please try again later.', 'Error processing request!', {
              //   timeOut: 0
              // });
              break;
            }
        }
      }

      /**
       * Otherwise, default behaviour
       */
      return $q.reject(rejection);
    }
  }
})();
