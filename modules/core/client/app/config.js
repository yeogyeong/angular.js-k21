((window) => {

  /**
   * Angular js application name
   */
  const applicationModuleName = 'onestopedu';

  /**
   * Initialize module configuration options
   */
  const service = {
    applicationEnvironment: window.env,
    applicationModuleVendorDependencies: [
      'checklist-model',
      'angularModalService',
      'ngAnimate',
      'chart.js',
      'ngMessages',
      'ngResource',
      'ngSanitize',
      'ui.router',
      'ui.router.state.events',
      'ngFileUpload',
      'ngMask',
      'ui.bootstrap'
    ],
    applicationModuleName,
    registerModule
  };

  /**
   * Initialize root module configuration
   */
  window.ApplicationConfiguration = service;

  /**
   * Add a new vertical module
   */
  function registerModule(moduleName, dependencies = []) {
    angular.module(moduleName, dependencies);
    angular.module(applicationModuleName).requires.push(moduleName);
  }
})(window);
