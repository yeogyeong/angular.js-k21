(function() {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .directive('onlyNumber', onlyNumber);

  /**
   * Users directive used to force onlyNumber input
   */
  function onlyNumber() {
    const directive = {
      require: 'ngModel',
      link
    };

    return directive;

    function link(scope, element, attrs, modelCtrl) {
      modelCtrl.$parsers.push((input) => {
        const numberInput = input.replace(/[^0-9]/g, '');
        if (numberInput !== input) {
          modelCtrl.$setViewValue(numberInput);
          modelCtrl.$render();
        }
        return numberInput;
      });
    }
  }
}());
