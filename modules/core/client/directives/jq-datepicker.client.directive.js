(function() {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .directive('jqDatepicker', jqDatepicker);
  jqDatepicker.$inject = ['$window'];

  /**
   * Directive for JQuery UI datepicker
   */
  function jqDatepicker($window) {
    const directive = {
      restrict: 'A',
      require: 'ngModel',
      link
    };

    return directive;

    function link(scope, elem, attrs, ngModel) {
      // const datepickerElement = elem[0];
      const koreanDaysName = ['일', '월', '화', '수', '목', '금', '토'];
      const koreanMonthName = ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'];

      elem.datepicker({
        showOn: 'both',
        buttonImage: '/img/ico_calendar.png',
        buttonImageOnly: true,
        dateFormat: 'yy-mm-dd',
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: koreanMonthName,
        monthNamesShort: koreanMonthName,
        dayNames: koreanDaysName,
        dayNamesShort: koreanDaysName,
        dayNamesMin: koreanDaysName,
        changeYear: true,
        changeMonth: true,
        showMonthAfterYear: true,
        minDate: 0,
        beforeShowDay: $.datepicker.noWeekends,
        // defaultDate: new Date(),
        onSelect
      });


      function onSelect(date) {
        // console.log(date);
        ngModel.$setViewValue(date);
        scope.$broadcast('ui:datepicker.desiredate', date);
        scope.$apply();
      }

      // Set date
      elem.datepicker('setDate', 'today');
    }
  }
}());
