(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .directive('scrollTop', scrollTop);

  /**
   * Directive show error
   */
  function scrollTop() {
    const directive = {
      restrict: 'A',
      require: '^form',
      link
    };

    return directive;

    function link(scope, elem, attrs, formCtrl) {
      elem.on('click', (e) => {
        if (formCtrl && formCtrl.$invalid) {
          angular.element('html, body')
            .animate({
              scrollTop: angular.element(`#contents`).offset().top
            }, 1000);
        }
      });
    }
  }
})();
