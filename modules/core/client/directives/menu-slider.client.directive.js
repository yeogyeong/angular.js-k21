(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .directive('menuSlider', menuSlider);

  /**
   * Directive show error
   */
  function menuSlider() {
    const directive = {
      restrict: 'A',
      link
    };

    return directive;

    function link(scope, headerElement) {

      const headerHeight = headerElement.height();
      const globalNaviHeight = angular.element('.gnb_sub').height();
      const innerElement = angular.element('.inner');
      const subMenuElement = angular.element('.gnb_sub, .mymenu > ul');

      innerElement.on('mouseenter', () => {
        subMenuElement.slideDown({
          duration: 400,
          easing: 'easeInOutExpo'
        });

        headerElement
          .stop()
          .animate({
            height: headerHeight + (globalNaviHeight + 30)
          }, 400);

      });

      headerElement.on('mouseleave', () => {
        subMenuElement
          .stop()
          .slideUp({
            duration: 400,
            easing: 'easeInOutExpo'
          });

        headerElement
          .stop()
          .animate({
            height: headerHeight
          }, 400);

      });
    }
  }
})();
