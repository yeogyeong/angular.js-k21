(function() {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .directive('coupon', coupon);

  /**
   * Users directive used to force lowercase input
   */
  function coupon() {
    const directive = {
      require: 'ngModel',
      link
    };

    return directive;

    function link(scope, element, attrs, modelCtrl) {
      const strValid = /^[0-9a-zA-Z]*$/;
      modelCtrl.$parsers.push((input) => {
          if (!strValid.test(input)) {
            element[0].value = input.replace(/[^0-9a-zA-Z]/g, '');
        } else {
          return input;
        }
      });
    }
  }
}());
