(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .controller('HomeController', HomeController);

  /**
   * Dependency Injection
   */
  HomeController.$inject = [
    'Authentication',
    'NoticeService',
    'CourseReviewService'
  ];

  /**
   * Configuring the header controller
   */
  function HomeController(
    Authentication,
    NoticeService,
    CourseReviewService
  ) {

    const vm = this;

    vm.auth = Authentication;

    CourseReviewService.query((homeCourserReviews) => {
     vm.homeCourserReviews = homeCourserReviews;
    });

    NoticeService.noticeGet((homeNotices) => {
      vm.homeNotices = homeNotices;
    });

    angular.element('.snb > ul > li > a').autoscroll = true;
  }

})();
