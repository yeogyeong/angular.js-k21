(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .controller('HeaderController', HeaderController);

  /**
   * Dependency Injection
   */
  HeaderController.$inject = [
    'Authentication',
    '$window',
    '$state'
  ];

  /**
   * Configuring the header controller
   */
  function HeaderController(
    Authentication,
    $window,
    $state
  ) {

    const vm = this;
    vm.auth = Authentication;
    vm.mypageCourse = mypageCourse;
    vm.mypageLevel = mypageLevel;

    function mypageCourse() {
      $window.alert('로그인이 필요한 서비스 입니다.');
      vm.auth.clickInPlace = 'mypageCourse';
      $state.go('user.signin');
      return false;
    }

    function mypageLevel() {
      $window.alert('로그인이 필요한 서비스 입니다.');
      vm.auth.clickInPlace = 'mypageLevelTest';
      $state.go('user.signin');
      return false;
    }

  }

})();
