(function(app) {
  app.registerModule('core');
  app.registerModule('core.routes', ['ui.router']);
}(ApplicationConfiguration));
