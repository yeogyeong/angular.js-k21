(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .filter('telHyphen', telHyphen);

  function telHyphen() {

    return ((number) => {
      if (number) {
        let tel = '';
        let seoul = 0;

        if (number.substring(0, 2).indexOf('02') === 0) {
          seoul = 1;
        }

        if (number.length < (4 - seoul)) {
          return number;
        } else if (number.length < (11 - seoul)) {
          tel += number.substr(0, (3 - seoul));
          tel += '-';
          tel += number.substr((3 - seoul), 3);
          tel += '-';
          tel += number.substr(6 - seoul);
        } else {
          tel += number.substr(0, (3 - seoul));
          tel += '-';
          tel += number.substr((3 - seoul), 4);
          tel += '-';
          tel += number.substr(7 - seoul);
        }
        number = tel;
        return number;
      }


    });
  }

})();
