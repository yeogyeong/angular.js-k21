(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .filter('url', url);

  /**
   * Dependency Injection
   */
  url.$inject = ['$sce'];

  /**
   * Remove TAG </tag>
   */
  function url($sce) {

    return (input) => {
      return input ? $sce.trustAsResourceUrl(input) : '';
    };
  }

})();
