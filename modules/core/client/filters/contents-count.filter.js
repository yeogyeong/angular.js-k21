(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .filter('contentsCount', contentsCount);

  function contentsCount() {
    const vm = this;
    const pattern = /.{0}$/;

    return ((text) => {

      const textLength = text.length;

      if (textLength >= 90) {
        return text.substring(0, 90).replace(pattern, '...');
      } else {
        return text;
      }

    });
  }

})();
