(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .filter('money', money);

  function money() {
    return ((number) => {
      return number.toLocaleString();
    });
  }

})();
