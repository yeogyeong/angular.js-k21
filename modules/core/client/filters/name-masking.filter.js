(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .filter('nameMasking', nameMasking);

  function nameMasking() {
    const pattern = /.{1}$/;
    return ((text) => {
      return text.replace(pattern, '*');
    });
  }

})();
