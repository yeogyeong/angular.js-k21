(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .filter('letterCount', letterCount);

  function letterCount() {
    const vm = this;
    const pattern = /.{0}$/;

    return ((text) => {

      const textLength = text.length;

      if (textLength >= 15) {
        return text.substring(0, 15).replace(pattern, '...');
      } else {
        return text;
      }

    });
  }

})();
