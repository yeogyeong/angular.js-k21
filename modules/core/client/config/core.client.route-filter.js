(() => {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .run(routeFilter);

  /**
   * Dependency Injection
   */
  routeFilter.$inject = ['$rootScope', '$state', 'Authentication'];

  /**
   * Setting up route filter
   */
  function routeFilter($rootScope, $state, Authentication) {
    $rootScope.$on('$stateChangeStart', stateChangeStart);
    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);

    function stateChangeStart(event, toState, toParams, fromState, fromParams) {

      /**
       * Check authentication before changing state
       */
      if (toState.data && toState.data.roles && toState.data.roles.length > 0) {
        let allowed = false;
        for (let i = 0, roles = toState.data.roles; i < roles.length; i += 1) {
          if ((roles[i] === 'guest') || (Authentication.user && Authentication.user.roles !== undefined && Authentication.user.roles.indexOf(roles[i]) !== -1)) {
            allowed = true;
            break;
          }
        }

        if (!allowed) {
          event.preventDefault();
          if (Authentication.user !== null && typeof Authentication.user === 'object') {
            $state.transitionTo('forbidden');
          } else {
            $state.go('home').then(() => {

              /**
               * Record previous state
               */
              storePreviousState(toState, toParams);
            });
          }
        }
      }
    }

    function stateChangeSuccess(event, toState, toParams, fromState, fromParams) {
      storePreviousState(fromState, fromParams);

      /**
       * Scroll top when page is changed
       */
      angular.element('html,body').animate({
        scrollTop: angular.element('html,body').offset().top
      }, 0);
    }

    /**
     * Store previous state
     */
    function storePreviousState(state, params) {

      if (!state.data || !state.data.ignoreState) {
        $state.previous = {
          state,
          params,
          href: $state.href(state, params)
        };
      }



    }
  }
})();
