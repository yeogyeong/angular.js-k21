(function() {

  /**
   * Module Configuration
   */
  angular
    .module('user.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('user', {
        abstract: true,
        url: '/user',
        template: '<ui-view/>'
      })
      /**
       * user
       */
      .state('user.find-password', {
        url: '/find-password',
        templateUrl: 'modules/user/client/views/find-password.client.view.html',
        controller: 'FindPasswordController',
        controllerAs: 'vm'
      })
      .state('user.find-username', {
        url: '/find-username',
        templateUrl: 'modules/user/client/views/find-username.client.view.html',
        controller: 'FindUserNameController',
        controllerAs: 'vm'
      })
      .state('user.signin', {
        url: '/signin',
        templateUrl: 'modules/user/client/views/signin.client.view.html',
        controller: 'SigninController',
        controllerAs: 'vm'
      })
      .state('user.signup', {
        url: '/signup',
        templateUrl: 'modules/user/client/views/signup.client.view.html',
        controller: 'SignupUserController',
        controllerAs: 'vm'
      })
      .state('user.signup-complete', {
        url: '/signup-complete',
        templateUrl: 'modules/user/client/views/signup-complete.client.view.html'
      });
  }

}());
