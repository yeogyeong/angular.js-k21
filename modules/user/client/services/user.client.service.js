(function() {

  /**
   * Module Configuration
   */
  angular
    .module('user.services')
    .factory('UserService', UserService);

  /**
   * Dependency Injection
   */
  UserService.$inject = ['$resource'];


  function UserService($resource) {
    const service = $resource('/api/user', {}, {
      update: {
        method: 'PUT'
      },
      signin: {
        method: 'POST',
        url: '/api/signIn'
      },
      signup: {
        method: 'POST',
        url: '/api/signUp'
      },
      checkId: {
        method: 'POST',
        url: '/api/checkId'
      },
      forgotById: {
        method: 'POST',
        url: '/api/forgotById'
      },
      forgotByPassword: {
        method: 'POST',
        url: '/api/forgotByPassword'
      }
    });


    angular.extend(service, {
      signupUser,
      signinUser,
      findUniqueUsername,
      findId,
      findPassword
    });

    return service;


    function signupUser(credentials) {
      return this.signup(credentials).$promise;
    }

    function signinUser(credentials) {
      return this.signin(credentials).$promise;
    }

    function findUniqueUsername(credentials) {
      return this.checkId({
        username: credentials.username
      }).$promise;
    }

    function findId(user) {
      return this.forgotById(user).$promise;
    }

    function findPassword(user) {
      return this.forgotByPassword(user).$promise;
    }



  }
}());
