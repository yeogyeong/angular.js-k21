(function(app) {
  app.registerModule('user', ['core']);
  app.registerModule('user.services');
  app.registerModule('user.routes', ['core.routes', 'user.services']);
}(ApplicationConfiguration));
