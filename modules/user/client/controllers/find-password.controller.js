(function() {

    /**
     * Module Configuration
     */
    angular
      .module('user.routes')
      .controller('FindPasswordController', FindPasswordController);

    /**
     * Dependency Injection
     */
    FindPasswordController.$inject = [
      '$scope',
      '$window',
      'UserService'
    ];

    /**
     * Find in controller
     */
    function FindPasswordController(
      $scope,
      $window,
      UserService
    ) {

      const vm = this;
      vm.user = {};

      vm.findPassword = findPassword;
      vm.numberValid = numberValid;

      /**
       * When the phone number is not a number
       */
      function numberValid() {
        const mobileValid = /^[0-9]*$/;

        if (!mobileValid.test(vm.user.mobile) && vm.user.mobile) {
        vm.user.mobile = vm.user.mobile.replace(/[^0-9]/g, '');
        $window.alert('숫자만 입력 가능합니다.');
        return false;
      }
    }

    function findPassword(isValid) {

      /**
       * Validate form
       */
      if (!isValid) {
        vm.isFindPassword = true;
        $scope.$broadcast('show-errors-check-validity', 'vm.form.findPasswordForm');
        return false;
      }

      /**
       * UserService findPassword
       */

      UserService.findPassword(vm.user)
        .then(findPasswordSuccess)
        .catch(findPasswordError);

      /**
       * UserService findPassword Success callback
       */
      function findPasswordSuccess(success) {
        vm.isFindPassword = true;
        vm.isPassword = true;
      }


      /**
       * UserService findPassword Error callback
       */
      function findPasswordError(err) {
        vm.isFindPassword = true;
        vm.isPassword = false;
        return false;
      }

    }
  }
}());
