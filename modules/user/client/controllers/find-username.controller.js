(function() {

  /**
   * Module Configuration
   */
  angular
    .module('user.routes')
    .controller('FindUserNameController', FindUserNameController);

  /**
   * Dependency Injection
   */
  FindUserNameController.$inject = [
    '$scope',
    '$window',
    'UserService'
  ];

  /**
   * Find in controller
   */
  function FindUserNameController(
    $scope,
    $window,
    UserService
  ) {

    const vm = this;
    vm.user = {};

    vm.findUsername = findUsername;
    vm.numberValid = numberValid;

    /**
     * When the phone number is not a number
     */
    function numberValid() {
      const mobileValid = /^[0-9]*$/;

      if (!mobileValid.test(vm.user.mobile) && vm.user.mobile) {
        vm.user.mobile = vm.user.mobile.replace(/[^0-9]/g, '');
        $window.alert('숫자만 입력 가능합니다.');
        return false;
      }
  }

    function findUsername(isValid) {

      /**
       * Validate form
       */
      if (!isValid) {
        vm.isFindUsername = true;
        $scope.$broadcast('show-errors-check-validity', 'vm.form.findUsernameForm');
        return false;
      }


      /**
       * UserService findId
       */
      UserService.findId(vm.user)
        .then(findIdSuccess)
        .catch(findIdError);


      /**
       * UserService findId Success callback
       */
      function findIdSuccess(success) {
        vm.isFindUsername = true;
        vm.isRegistered = true;
        vm.user.username = success.username;
      }

      /**
       * UserService findId Error callback
       */
      function findIdError(err) {
        vm.isFindUsername = true;
        vm.isRegistered = false;
        return false;
      }
    }

  }
}());
