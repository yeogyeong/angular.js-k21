(function() {

  /**
   * Module Configuration
   */
  angular
    .module('user.routes')
    .controller('SignupUserController', SignupUserController);

  /**
   * Dependency Injection
   */
  SignupUserController.$inject = [
    '$scope',
    '$window',
    '$state',
    'UserService'
  ];

  /**
   * Configuring the User signup controller
   */
  function SignupUserController(
    $scope,
    $window,
    $state,
    UserService
  ) {

    const vm = this;

    vm.credentials = {};
    vm.signup = signup;

    vm.findUniqueUsername = findUniqueUsername;
    vm.isFindUniqueUsername = false;

    vm.terms = [];
    vm.termsChanged = termsChanged;
    vm.termsAllChanged = termsAllChanged;

    vm.userNameRex = validateUsername();
    vm.koreanNameRex = validateKoreanName();
    vm.englishNameRex = validateEnglishName();
    vm.mobileRex = validateMobile();
    vm.emailRex = validateEmail();


    $scope.$watch('vm.credentials.username', (username) => {
      if (username && vm.checkedUsername) {
        vm.isFindUniqueUsername = (username === vm.checkedUsername);
      }
    });



    /**
     * Validation function for username
     */
    function validateUsername() {
      return new RegExp(/^[a-zA-Z0-9]*$/);
    }

    /**
     * Validation function for korean name
     */
    function validateKoreanName() {
      return new RegExp(/^[a-zA-Z가-힣]*$/);
    }

    /**
     * Validation function for englishn name
     */
    function validateEnglishName() {
      return new RegExp(/^[a-zA-Z]*$/);
    }

    /**
     * Validation function for mobile
     */
    function validateMobile() {
      return new RegExp(/^[0-9]*$/);
    }

    /**
     * Validation function for e-mail
     */
    function validateEmail() {
      return new RegExp(/^(\w+\.)*\w+@(\w+\.)+[A-Za-z]+/);
    }

    /**
     * Changed event callback function checkbox of terms
     */
    function termsChanged() {
      vm.isTermsAllChecked = (vm.terms.length > 1);
    }

    /**
     * Changed event callback function checkbox of all terms checked
     */
    function termsAllChanged() {
      vm.terms = (vm.isTermsAllChecked) ? ['use', 'personal'] : [];
    }

    /**
     * Click event callback for find username
     */
    function findUniqueUsername() {
      vm.isUsernameError = false;
      /**
       * Validate form
       */
      if (!vm.form.signupForm.username.$valid) {
        $scope.$broadcast('show-errors-username-check-validity', 'vm.form.signupForm');
        return false;
      }

      UserService.findUniqueUsername(vm.credentials)
        .then(onFindUniqueUsernameSuccess)
        .catch(onFindUniqueUsernameError);

      /**
       * Find unique username success callback
       */
      function onFindUniqueUsernameSuccess(findResponse) {
        if (findResponse) {
          vm.isFindUniqueUsername = true;
          vm.checkedUsername = vm.credentials.username;

        }
      }

      /**
       * Find unique username error callback
       */
      function onFindUniqueUsernameError(findError) {
        if (findError) {
          vm.isFindUniqueUsername = false;
          vm.isUsernameError = true;
          vm.credentials.username = '';
        }
      }


    }


    /**
     * Sign up user
     */
    function signup(isValid) {

      /**
       * credentials mobile
       */
      vm.credentials.mobile = vm.mobileFirst + vm.mobileMiddle + vm.mobileLast;

      if (!isValid || !vm.isTermsAllChecked) {
        notCompleteValid();
      } else {
        completeValid();
      }

      function notCompleteValid() {
        /**
         * Validate form
         */
        if (!vm.credentials.username) {
          $scope.$broadcast('show-errors-check-validity', 'vm.form.signupForm');
        } else if (!vm.isFindUniqueUsername) {
          $window.alert('아이디 중복확인을 눌러주세요');
          scrollTop('#idCheck');
        } else if (!isValid) {
          $scope.$broadcast('show-errors-check-validity', 'vm.form.signupForm');
        } else if (!vm.isTermsAllChecked || vm.terms.length < 2) {
          $window.alert('전체 이용약관을 체크해주세요');
          scrollTop('#termsAll');
        }
        return false;
      }


      function scrollTop(id) {
        angular.element('html,body').animate({
          scrollTop: angular.element(id).offset().top - 50
        }, 100);
      }


      function completeValid() {

        if ($window.confirm('회원가입을 하시겠습니까?')) {

          UserService.signupUser(vm.credentials)
            .then(onSignupUserSuccess)
            .catch(onSignupUserError);
        }
      }

      /**
       * Sign up user success callback
       */
      function onSignupUserSuccess(user) {
        $state.go('user.signup-complete');
      }

      /**
       * Sign up user error callback
       */
      function onSignupUserError(err) {
        $window.alert('회원가입을 실패하셨습니다');
      }
    }

  }
}());
