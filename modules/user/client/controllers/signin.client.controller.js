(function() {

  /**
   * Module Configuration
   */
  angular
    .module('user.routes')
    .controller('SigninController', SigninController);

  /**
   * Dependency Injection
   */
  SigninController.$inject = [
    '$scope',
    '$window',
    '$state',
    '$location',
    '$timeout',
    'UserService',
    'Authentication'
  ];

  /**
   * Sign in controller
   */
  function SigninController(
    $scope,
    $window,
    $state,
    $location,
    $timeout,
    UserService,
    AuthService
  ) {

    const vm = this;

    vm.auth = AuthService;
    vm.user = {};
    vm.signin = signin;
    vm.isSigninError = false;


    // If user is signed in then redirect back home
    if (vm.auth.user) {
      $location.path('/');
    }

    /**
     * Sign in user
     */
    function signin(isValid) {
      vm.isSigninError = true;

      /**
       * Remove text over time
       */
      const removeError = function() {
        vm.isSigninError = false;
      };


      /**
       * Sign in
       */
      UserService.signinUser(vm.user)
        .then(signInSuccess)
        .catch(signInError);


      /**
       * Sign in Success callback
       */
      function signInSuccess(user) {
        vm.auth.user = user;
        if (vm.auth.clickInPlace === 'courseReview') {
          $state.go('articles.course-review.write');
        } else if (vm.auth.clickInPlace === 'contactUs') {
          $state.go('articles.contact-us.write');
        } else if (vm.auth.clickInPlace === 'mypageCourse') {
          $state.go('mypage.course');
        } else if (vm.auth.clickInPlace === 'mypageLevelTest') {
          $state.go('mypage.leveltest.list');
        } else {
          $state.go('home');
        }
      }

      /**
       * Sign in Error callback
       */
      function signInError(err) {
        vm.user.username = '';
        vm.user.password = '';
        angular.element('#username').focus();

        $timeout(removeError, 5000);

        return false;
      }
    }
  }
}());
