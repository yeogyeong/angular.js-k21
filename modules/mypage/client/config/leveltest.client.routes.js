(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('mypage.leveltest', {
        abstract: true,
        url: '/leveltest',
        template: '<ui-view/>'
      })
      /**
       * leveltest
       */
       .state('mypage.leveltest.list', {
         url: '/list?:page',
         templateUrl: 'modules/mypage/client/views/leveltest/list-leveltest.client.view.html',
         controller: 'ListMyPageLevelController',
         controllerAs: 'vm'
       })
       .state('mypage.leveltest.list.read', {
         url: '/:leveltestId/read',
         templateUrl: 'modules/mypage/client/views/leveltest/read-member-leveltest.client.view.html',
         controller: 'ReadMemberLevelController',
         controllerAs: 'vm',
         resolve: {
           leveltestResolve: getLeveltest
         }
       })
       .state('mypage.leveltest.guest', {
         url: '/guest',
         templateUrl: 'modules/mypage/client/views/leveltest/list-guest-leveltest.client.view.html',
         controller: 'GuestLeveltestController',
         controllerAs: 'vm'
       })
       .state('mypage.leveltest.guest.read', {
         url: '/read',
         templateUrl: 'modules/mypage/client/views/leveltest/read-guest-leveltest.client.view.html',
         controller: 'ReadGuestLevelController',
         controllerAs: 'vm'
       });

       /**
        * Dependency Injection
        */
       getLeveltest.$inject = ['$stateParams', 'MyPageLevelService'];

       /**
        * Get Leveltest resolve
        */
       function getLeveltest($stateParams, MyPageLevelService) {
         return MyPageLevelService.get({
           leveltestId: $stateParams.leveltestId
         }).$promise;
       }

  }

}());
