(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('mypage', {
        abstract: true,
        url: '/mypage',
        template: '<ui-view/>'
      })
      /**
       * course
       */
      .state('mypage.course', {
        url: '/course',
        templateUrl: 'modules/mypage/client/views/course/list-course.client.view.html',
        controller: 'CourseController',
        controllerAs: 'vm'
      })
      /**
       * payment
       */
      .state('mypage.payment', {
        url: '/payment?:page',
        templateUrl: 'modules/mypage/client/views/payment/list-payment.client.view.html',
        controller: 'PaymentController',
        controllerAs: 'vm'
      })
      /**
       * coupon
       */
       .state('mypage.coupon', {
         url: '/coupon?:page',
         templateUrl: 'modules/mypage/client/views/coupon/list-coupon.client.view.html',
         controller: 'CouponController',
         controllerAs: 'vm'
       })
      /**
       * point
       */
      .state('mypage.point', {
        url: '/point?:page',
        templateUrl: 'modules/mypage/client/views/point/list-point.client.view.html',
        controller: 'PointController',
        controllerAs: 'vm'
      })
      /**
       * member
       */
      .state('mypage.member', {
        abstract: true,
        url: '/member',
        template: '<ui-view/>'
      })
      .state('mypage.member.modify', {
        url: '/modify',
        templateUrl: 'modules/mypage/client/views/member/modify-member.client.view.html',
        controller: 'ModifyMemberController',
        controllerAs: 'vm'
      })
      .state('mypage.member.withdrawal', {
        url: '/withdrawal',
        templateUrl: 'modules/mypage/client/views/member/withdrawal-member.client.view.html',
        controller: 'withdrawalController',
        controllerAs: 'vm'
      })
      .state('mypage.member.withdrawal-confirm', {
        url: '/withdrawal-confirm',
        templateUrl: 'modules/mypage/client/views/member/withdrawal-confirm-member.client.view.html'
      });
  }

}());
