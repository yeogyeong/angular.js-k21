(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('mypage.evaluation', {
        abstract: true,
        url: '/evaluation',
        template: '<ui-view/>'
      })
      /**
       * evaluation
       */
       .state('mypage.evaluation.list', {
         url: '/list?:page',
         templateUrl: 'modules/mypage/client/views/evaluation/list-evaluation.client.view.html',
         controller: 'ListMyPageEvaluationController',
         controllerAs: 'vm'
       })
       .state('mypage.evaluation.list.read', {
         url: '/:monthlyId/read',
         templateUrl: 'modules/mypage/client/views/evaluation/read-evaluation.client.view.html',
         controller: 'ReadEvaluationController',
         controllerAs: 'vm',
         resolve: {
           evaluationResolve: getEvaluation
         }
       });

       getEvaluation.$inject = ['$stateParams', 'myPageEvaluationService'];

       function getEvaluation($stateParams, myPageEvaluationService) {
         return myPageEvaluationService.get({
           monthlyId: $stateParams.monthlyId
         }).$promise;
       }
  }

}());
