(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .config(routeConfig);

  /**
   * Dependency Injection
   */
  routeConfig.$inject = ['$stateProvider'];

  /**
   * Setting up route
   */
  function routeConfig($stateProvider) {

    $stateProvider
      .state('mypage.consult', {
        abstract: true,
        url: '/consult',
        template: '<ui-view/>'
      })
      /**
       * consult
       */
       .state('mypage.consult.list', {
         url: '/list?:page&:searchOptions&:searchKeyword',
         templateUrl: 'modules/mypage/client/views/consult/list-consult.client.view.html',
         controller: 'ListMyPageConsultController',
         controllerAs: 'vm'
       })
       .state('mypage.consult.read', {
         url: '/:contact1x1Id/read',
         templateUrl: 'modules/mypage/client/views/consult/read-consult.client.view.html',
         controller: 'ReadMyPageConsultController',
         controllerAs: 'vm',
         resolve: {
           consultResolve: getConsult
         }
       })
       .state('mypage.consult.write', {
         url: '/write',
         templateUrl: 'modules/mypage/client/views/consult/write-consult.client.view.html',
         controller: 'WriteConsultController',
         controllerAs: 'vm',
         resolve: {
           consultResolve: newconsult
         }
       })
       .state('mypage.consult.edit', {
         url: '/:contact1x1Id/edit',
         templateUrl: 'modules/mypage/client/views/consult/edit-consult.client.view.html',
         controller: 'EditMyPageConsultController',
         controllerAs: 'vm',
         resolve: {
           consultResolve: getConsult
         }
       });

       /**
        * get Course Review
        */
         getConsult.$inject = ['$stateParams', 'MyPageConsultService'];

         function getConsult($stateParams, MyPageConsultService) {
           return MyPageConsultService.get({
             contact1x1Id: $stateParams.contact1x1Id
           }).$promise;
         }

       newconsult.$inject = ['MyPageConsultService'];

       function newconsult(MyPageConsultService) {
         return new MyPageConsultService();
       }
  }

}());
