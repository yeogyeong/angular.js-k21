(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .directive('mypageCaleandar', mypageCaleandar);

  mypageCaleandar.$inject = [
    '$timeout',
    'MypageCourseService',
    'Authentication',
    'ModalService',
    'MyPageLevelService'
  ];

  /**
   * Directive apply for mypage course caleandar
   */
  function mypageCaleandar(
    $timeout,
    MypageCourseService,
    Authentication,
    ModalService,
    MyPageLevelService
  ) {

    const directive = {
      restrict: 'A',
      templateUrl: './modules/mypage/client/templates/mypage-calendar.client.view.html',
      require: ['^mypageCaleandar'],
      scope: {
        scheduled: '=scheduled',
        months: '=months',
        isRecording: '=isRecording',
        recordId: '=recordId'
      },
      controller,
      controllerAs: 'ctrl',
      link
    };

    return directive;

    function link(scope, element, attrs) {
      scope.auth = Authentication.user;
      scope.attendance = attendance;

      function attendance(schedule) {
        // console.log(schedule);
        MypageCourseService.feedbackList({
            feedbackId: schedule._id.feedback
          })
          .then((lesson) => {
            // console.log(lesson);
            scope.feedback = lesson.feedback;
            scope.feedback.tutorFeedback = scope.feedback.tutorFeedback.replace(/(?:\r\n|\r|\n)/g, '<br />');
            scope.course = lesson.course;
            scope.lessonTime = moment(lesson.started).format('hh:mm') + '~' + moment(lesson.ended).format('hh:mm');

            scope.recordId = scope.feedback.recordFile;
          })
          .catch((err) => {
            console.log(err);
          });

        scope.isFeedback = true;
      }


      /**
       * Records download
       */
      scope.recording = recording;

      /**
       * Records download
       */
      function recording() {
        scope.isRecording = true;
      }


    }

    /**
     * Controller
     */
    function controller($scope) {
      const ctrl = this;

      ctrl.calendar = moment();
      ctrl.weekName = ctrl.calendar._locale._weekdaysShort;

      const startDay = () => Number(ctrl.calendar.startOf('month').day());
      const endDay = () => Number(ctrl.calendar.endOf('month').format('D'));

      /**
       * Create a Day's Number
       */
      ctrl.thisMonthDay = function() {
        const days = [];
        let dayNumber = 1;

        for (let day = 0; day < endDay() + startDay(); day += 1) {
          if (day >= startDay()) {
            days.push(dayNumber);
            dayNumber += 1;
          } else if (day < startDay()) {
            days[day] = '';
          }
        }
        ctrl.thisMonthWeek(days);

        return days;
      };

      /**
       * Create a Week
       */
      ctrl.thisMonthWeek = function(days) {
        const weeks = [];
        const sevenDays = Math.floor(endDay() / 7);
        for (let i = 0; i <= sevenDays; i += 1) {
          weeks.push(days.splice(0, 7));
        }
        ctrl.weeks = weeks;

        return weeks;
      };

      ctrl.rows = ctrl.thisMonthDay();


      /**
       * Last Month's Schedule
       */
      ctrl.lastMonth = lastMonth;

      function lastMonth() {
        ctrl.calendar.subtract(1, 'month').format('MM');
        $scope.months = ctrl.calendar.format('YYYY-MM');
        if (!$scope.scheduled) {
          ctrl.rows = ctrl.thisMonthDay();
        }
      }

      /**
       * Next Month's Schedule
       */
      ctrl.nextMonth = nextMonth;

      function nextMonth() {
        ctrl.calendar.add(1, 'month').format('MM');
        $scope.months = ctrl.calendar.format('YYYY-MM');
        if (!$scope.scheduled) {
          ctrl.rows = ctrl.thisMonthDay();
        }
      }

      /**
       * Get Changing Schedule data
       */
      $scope.$watch('scheduled', function(scheduled) {
        if (scheduled) {
          getSchedule(scheduled);
        }
      });

      /**
       * Get Schedule data function
       */
      function getSchedule(scheduled) {
        ctrl.scheduled = scheduled;
        ctrl.scheduled.forEach((item) => {
          const scheduleDay = moment(item._id.started).format('DD');
          const scheduleTime = moment(item._id.started).format('hh:mm') + '~' + moment(item._id.ended).format('hh:mm');
          item._id.day = +scheduleDay;
          item._id.time = scheduleTime;

          /**
           * Reset when selectBox is selected
           */
          if (moment(item._id.started).format('YYYY-MM') !== $scope.months) {
            ctrl.calendar = moment(item._id.started);
          }
        });
        ctrl.rows = ctrl.thisMonthDay();
      }

    }

  }


}());
