(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .directive('ngPrint', ngPrint);

  /**
   * Users directive receipt
   */
  function ngPrint() {
    const directive = {
      restrict: 'A',
      scope: {
        printElementId: '=printElementId'
      },
      link
    };

    let printSection = document.getElementById('printSection');

    if (!printSection) {
      printSection = document.createElement('div');
      printSection.id = 'printSection';
      document.body.appendChild(printSection);
    }

    return directive;

    function link(scope, element, attrs) {
      element.on('click', function() {
        const elemToPrint = document.getElementById(attrs.printElementId);

        window.onafterprint = function() {
          printSection.innerHTML = '';
        };

        if (elemToPrint) {
          printElement(elemToPrint);
        }

        function printElement(elem) {
          const domClone = elem.cloneNode(true);
          if (printSection.innerHTML === '') {
            printSection.appendChild(domClone);
          }
          window.print();
        }

      });
    }




  }
}());
