(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .directive('recordingFile', recordingFile);

  recordingFile.inject = ['MyPageLevelService', '$timeout'];
  /**
   * Directive apply for mypage Recording File
   */
  function recordingFile(MyPageLevelService, $timeout) {

    const directive = {
      restrict: 'A',
      templateUrl: './modules/mypage/client/templates/recording-file.client.html',
      require: ['^recordingFile'],
      scope: {
        recordFileId: '@recordFileId',
        isRecording: '=isRecording'
      },
      controller,
      controllerAs: 'ctrl',
      link
    };

    return directive;


    function link(scope) {
      MyPageLevelService.fileList(scope.recordFileId)
        .then((fileItemOption) => {
          // console.log(fileItemOption);
          scope.downloadUrl = fileItemOption.download + fileItemOption._id;
          scope.originalname = fileItemOption.originalname;
          scope.recordingUrl = fileItemOption.path;
        })
        .catch((error) => {
          console.log(error);
        });
    }

    /**
     * Controller
     */
    function controller($scope) {
      const ctrl = this;
      /**
       * Close
       */
      ctrl.dismiss = dismiss;

      /**
       * Close
       */
      function dismiss() {
        $scope.isRecording = false;
      }
    }

  }


}());
