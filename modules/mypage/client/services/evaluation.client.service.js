(function() {
  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .factory('myPageEvaluationService', myPageEvaluationService);

  /**
   * Dependency Injection
   */
  myPageEvaluationService.$inject = ['$resource'];

  function myPageEvaluationService($resource) {
    const service = $resource('/api/mypage/monthly/:monthlyId', {
      monthlyId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      monthlyTotalCount: {
        method: 'GET',
        url: '/api/mypage/monthlyTotalCount'
      },
      monthlyGet: {
        method: 'GET',
        isArray: true
      }
    });

    angular.extend(service, {
      monthlyList
    });

    /**
     * Services for importing data
     */
    function monthlyList(page) {
      return this.monthlyGet({
        page
      }).$promise;
    }

    return service;
  }
}());
