(function() {
  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .factory('CouponService', CouponService);

  /**
   * Dependency Injection
   */
  CouponService.$inject = ['$resource'];

  function CouponService($resource) {
    const service = $resource('/api/mypage/coupon', {
    }, {
      update: {
        method: 'PUT'
      },
      couponPost: {
        method: 'POST',
        isArray: true
      },
      couponTotalCount: {
        method: 'GET',
        url: '/api/mypage/couponTotalCount'
      },
      couponGet: {
        method: 'GET',
        isArray: true
      }
      });

      angular.extend(service, {
      couponList,
      createCoupon
      });

      /**
      * Services for importing data
      */
      function couponList(page) {
      return this.couponGet({
        page
      }).$promise;
      }

      function createCoupon(code) {
      return this.couponPost(code).$promise;
      }

      return service;
  }
}());
