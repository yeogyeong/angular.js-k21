(function() {
  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .factory('MypageCourseService', MypageCourseService);

  /**
   * Dependency Injection
   */
  MypageCourseService.$inject = ['$resource'];

  function MypageCourseService($resource) {
    const service = $resource('/api/mypage/course/:courseId', {
      courseId: '@_id'
    }, {
      calendar: {
        method: 'GET',
        isArray: true
      },
      feedback: {
        url: '/api/mypage/lesson/:feedbackId',
        method: 'GET'
      }
    });

    angular.extend(service, {
    calendarList,
    feedbackList
    });

    /**
    * Services for importing data
    */
    function calendarList(courseId, year, month) {
    return this.calendar({
      courseId,
      year,
      month
    }).$promise;
    }

    function feedbackList(feedbackId) {
    return this.feedback(feedbackId).$promise;
    }


    return service;
  }
}());
