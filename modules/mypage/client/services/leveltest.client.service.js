(function() {
    /**
     * Module Configuration
     */
    angular
      .module('mypage.services')
      .factory('MyPageLevelService', MyPageLevelService);

    /**
     * Dependency Injection
     */
    MyPageLevelService.$inject = ['$resource'];

    function MyPageLevelService($resource) {
      const service = $resource('/api/mypage/leveltest/:leveltestId', {
          leveltestId: '@_id'
        }, {
          update: {
            method: 'PUT'
          },
          leveltestTotalCount: {
            method: 'GET',
            url: '/api/mypage/leveltestTotalCount'
          },
          leveltestGet: {
            method: 'GET',
            isArray: true
          },
          fileItem: {
            method: 'GET',
            url: '/api/file-item/:fileItemId'
          }
      });

      angular.extend(service, {
        leveltestList,
        fileList
      });

      /**
       * Services for importing data
       */
      function leveltestList(page) {
        return this.leveltestGet({
          page
        }).$promise;
      }

      function fileList(fileItemId) {
        return this.fileItem({
          fileItemId
        }).$promise;
      }


      return service;
  }
}());
