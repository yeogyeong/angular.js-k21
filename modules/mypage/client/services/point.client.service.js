(function() {
  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .factory('PointService', PointService);

  /**
   * Dependency Injection
   */
  PointService.$inject = ['$resource'];

  function PointService($resource) {
    const service = $resource('/api/mypage/point', {
    }, {
      update: {
        method: 'PUT'
      },
      pointTotalCount: {
        method: 'GET',
        url: '/api/mypage/pointTotalCount'
      },
      pointGet: {
        method: 'GET',
        isArray: true
      }
      });

      angular.extend(service, {
      pointList
      });

      /**
      * Services for importing data
      */
      function pointList(page) {
      return this.pointGet({
        page
      }).$promise;
      }

      return service;
  }
}());
