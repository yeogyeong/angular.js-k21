(function() {
  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .factory('MyPageConsultService', MyPageConsultService);

  /**
   * Dependency Injection
   */
  MyPageConsultService.$inject = ['$resource'];

  function MyPageConsultService($resource) {
    const service = $resource('/api/mypage/contact1x1/:contact1x1Id', {
      contact1x1Id: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      consultTotalCount: {
        method: 'GET',
        url: '/api/mypage/contact1x1TotalCount'
      },
      consultGet: {
        method: 'GET',
        isArray: true
      },
      mainTutor: {
        method: 'GET',
        url: '/api/mypage/mainTutor',
        isArray: true
      }
    });
    angular.extend(service, {
      consultList
    });

    /**
     * Services for importing data
     */
    function consultList(page, searchOptions, searchKeyword) {
      return this.consultGet({
        page,
        searchOptions,
        searchKeyword
      }).$promise;
    }

    return service;
  }
}());
