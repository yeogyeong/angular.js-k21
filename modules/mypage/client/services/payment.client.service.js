(function() {
  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .factory('PaymentService', PaymentService);

  /**
   * Dependency Injection
   */
  PaymentService.$inject = ['$resource'];

  function PaymentService($resource) {
    const service = $resource('/api/mypage/payment/:paymentId', {
      paymentId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      paymentTotalCount: {
        method: 'GET',
        url: '/api/mypage/paymentTotalCount'
      },
      paymentGet: {
        method: 'GET',
        isArray: true
      }
    });

    angular.extend(service, {
      paymentList
    });

    /**
     * Services for importing data
     */
    function paymentList(page) {
      return this.paymentGet({
        page
      }).$promise;
    }

    return service;

  }
}());
