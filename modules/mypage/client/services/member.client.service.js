(function() {
  /**
   * Module Configuration
   */
  angular
    .module('mypage.services')
    .factory('myPageService', myPageService);

  /**
   * Dependency Injection
   */
  myPageService.$inject = ['$resource'];

  function myPageService($resource) {
    return $resource('/api/mypage/member', {}, {
      modify: {
        method: 'POST',
        url: '/api/mypage/modify'
      },
      withdrawal: {
        method: 'POST',
        url: '/api/mypage/withdrawal/:withdrawalId'
      }
    });


  }
}());
