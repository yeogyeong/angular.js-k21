(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('withdrawalController', withdrawalController);

  /**
   * Dependency Injection
   */
  withdrawalController.$inject = [
    '$window',
    '$state',
    'myPageService',
    'Authentication'
  ];

  /**
   * Configuring the MyPage widhdrawal controller
   */
  function withdrawalController(
    $window,
    $state,
    myPageService,
    AuthService
  ) {
    const vm = this;

    vm.auth = AuthService;
    vm.complete = complete;
    vm.withdrawal = {};


    /**
     * When you are not signIn
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    }


    /**
     * Membership Withdrawal
     */
    function complete() {

      myPageService.withdrawal(vm.withdrawal)
        .$promise
        .then((res) => {
          AuthService.user = undefined;
          $state.go('mypage.member.withdrawal-confirm', {}, {
            reload: true
          });
        })
        .catch((err) => {
          if (err.status === 422) {
            $window.alert('정확한 비밀번호를 입력해주세요');
            return false;
          }
          return false;
        });
    }


  }

}());
