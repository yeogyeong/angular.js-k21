(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('ModifyMemberController', ModifyMemberController);

  /**
   * Dependency Injection
   */
  ModifyMemberController.$inject = [
    '$window',
    '$state',
    'myPageService',
    'Authentication'
  ];

  /**
   * Configuring the MyPage Payment list controller
   */
  function ModifyMemberController(
    $window,
    $state,
    myPageService,
    AuthService
  ) {
    const vm = this;

    vm.auth = AuthService;
    vm.completion = completion;


    /**
     * Scroll Top When the value does not meet the conditions
     */
    function scrollTop() {
      angular.element('html, body')
        .animate({
          scrollTop: angular.element(`#contents`).offset().top
        });
    }

    /**
     * When you are not signIn,
     * Replace Mobile Numeric
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    } else if (vm.auth.user.mobile.length === 11) {
      vm.mobileFirst = vm.auth.user.mobile.substring(0, 3);
      vm.mobileMiddle = vm.auth.user.mobile.substring(3, 7);
      vm.mobileLast = vm.auth.user.mobile.substring(7, 12);
    } else if (vm.auth.user.mobile.length === 10) {
      if (vm.auth.user.mobile.substring(0, 2) === '02') {
        vm.mobileFirst = vm.auth.user.mobile.substring(0, 2);
        vm.mobileMiddle = vm.auth.user.mobile.substring(2, 6);
        vm.mobileLast = vm.auth.user.mobile.substring(6, 11);
      } else {
        vm.mobileFirst = vm.auth.user.mobile.substring(0, 3);
        vm.mobileMiddle = vm.auth.user.mobile.substring(3, 6);
        vm.mobileLast = vm.auth.user.mobile.substring(6, 11);
      }
    } else if (vm.auth.user.mobile.length === 9) {
      vm.mobileFirst = vm.auth.user.mobile.substring(0, 2);
      vm.mobileMiddle = vm.auth.user.mobile.substring(2, 5);
      vm.mobileLast = vm.auth.user.mobile.substring(5, 9);
    }


    /**
     * When I pressed the completion button
     */
    function completion() {
      delete vm.auth.user._id;
      vm.auth.user.mobile = vm.mobileFirst + vm.mobileMiddle + vm.mobileLast;
      const emailRex = new RegExp(/^[A-Za-z0-9_.-]+@[A-Za-z0-9-]+.[A-Za-z0-9-]+/);

      /*
       * To ensure that the conditions are met
       */
      if (!vm.auth.user.currentPassword) {
        $window.alert('현재 비밀번호를 입력해 주세요');
        scrollTop();
        return false;
      } else if (vm.auth.user.newPassword || vm.auth.user.confirmPassword) {
        if (vm.auth.user.newPassword !== vm.auth.user.confirmPassword) {
          $window.alert('입력하신 비밀번호와 비밀번호 확인란의 비밀번호가 서로 다릅니다');
          scrollTop();
          return false;
        } else if (!(vm.auth.user.newPassword.length > 7 && vm.auth.user.newPassword.length < 20)) {
          $window.alert('형식에 맞는 비밀번호를 입력해주세요');
          scrollTop();
          return false;
        }
      } else if (!vm.auth.user.koreanName) {
        $window.alert('이름을 입력해주세요');
        scrollTop();
        return false;
      } else if (!vm.auth.user.englishName) {
        $window.alert('영어 닉네임을 입력해주세요');
        scrollTop();
        return false;
      } else if (!vm.auth.user.mobile) {
        $window.alert('잘못된 휴대폰번호 형식입니다');
        return false;
      } else if (!(vm.auth.user.email && emailRex.test(vm.auth.user.email))) {
        $window.alert('잘못된 이메일 형식입니다');
        return false;
      } else if (!vm.auth.user.address) {
        $window.alert('잘못된 주소 형식입니다');
        return false;
      }


      /**
       * When member information has been modified,
       * When the current password doesn't match
       */
      myPageService.modify(vm.auth.user)
        .$promise
        .then((success) => {
          $window.alert('회원정보가 수정되었습니다');
          $window.location.reload();
        })
        .catch((err) => {
          if (err.status === 422) {
            $window.alert('입력하신 현재 비밀번호가 맞지 않습니다');
            scrollTop();
            return false;
          }
        });
    }

  }

}());
