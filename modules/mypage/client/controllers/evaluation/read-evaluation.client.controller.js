(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('ReadEvaluationController', ReadEvaluationController);

  /**
   * Dependency Injection
   */
  ReadEvaluationController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'evaluationResolve'
  ];

  /**
   * Configuring the MyPage Leveltest list controller
   */
  function ReadEvaluationController(
    $window,
    $state,
    AuthService,
    evaluationResolve
  ) {
    const vm = this;
    vm.auth = AuthService;

    /**
     * Get MonthlyEvaluation
     */
    vm.evaluation = evaluationResolve;
    vm.evaluationRecord = vm.evaluation.evaluation;
    /**
     * courseInfo productType
     */
    vm.courseInfo = courseInfo;


    /**
     * Chart label
     */
    vm.labels = ['Listening', 'Speaking', 'Pronunciation', 'Vocabulary', 'Grammar'];

    vm.datasets = [{
      backgroundColor: 'rgba(255,255,255, 0)',
      borderColor: 'rgba(132,132,212, 1)'
    }];

    /**
     * Chart opionts
     */
    vm.chartOptions = {
      scale: {
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 100,
          stepSize: 20
        }
      }
    };


    /**
     * Chart dataset
     */
    vm.chartData = [
      [
        vm.evaluationRecord.listeningLevel = vm.evaluationRecord.listeningLevel ? (vm.evaluationRecord.listeningLevel * 10) : 10,
        vm.evaluationRecord.speakingLevel = vm.evaluationRecord.speakingLevel ? (vm.evaluationRecord.speakingLevel * 10) : 10,
        vm.evaluationRecord.pronunciationLevel = vm.evaluationRecord.pronunciationLevel ? (vm.evaluationRecord.pronunciationLevel * 10) : 10,
        vm.evaluationRecord.vocabularyLevel = vm.evaluationRecord.vocabularyLevel ? (vm.evaluationRecord.vocabularyLevel * 10) : 10,
        vm.evaluationRecord.grammarLevel = vm.evaluationRecord.grammarLevel ? (vm.evaluationRecord.grammarLevel * 10) : 10
      ]
    ];


    /**
     * When you are not signIn
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    }

    /**
     * courseInfo productType
     */
    function courseInfo(course) {
      course.productType = course.productType === 'telephone' ? '전화' : course.productType;
      course.productType = course.productType === 'skype' ? '스카이프' : course.productType;
      course.productType = course.productType === 'screenBoard' ? '화상칠판' : course.productType;
      return course.productType + ' 주 ' + course.payment.paymentedProduct.times + ' 회';
    }


  }

}());
