(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('ListMyPageEvaluationController', ListMyPageEvaluationController);

  /**
   * Dependency Injection
   */
  ListMyPageEvaluationController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'myPageEvaluationService'
  ];

  /**
   * Configuring the MyPage Leveltest list controller
   */
  function ListMyPageEvaluationController(
    $window,
    $state,
    AuthService,
    myPageEvaluationService
  ) {
    const vm = this;
    vm.auth = AuthService;
    vm.showEvaluation = showEvaluation;
    vm.pageChanged = pageChanged;


    /**
     * When you are not signIn
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    }

    /**
     * When there are no $state.params.page
     */
    if (!$state.params.page) {
      $state.go('mypage.evaluation.list', {
        page: 1
      });
    }

    /**
     * To import evaluation List data and create a list
     */
    myPageEvaluationService.monthlyList($state.params.page)
      .then((evaluations) => {
        vm.evaluations = evaluations;
        buildPager();
      })
      .catch((err) => {
        console.log(err);
      });

    /**
     * Build page
     */
    function buildPager() {
      myPageEvaluationService.monthlyTotalCount()
        .$promise
        .then((totalCount) => {
          vm.totalItems = totalCount.counts;
          vm.currentPage = $state.params.page;
          vm.itemsPerPage = 5;
        })
        .catch((err) => {
          console.log(err);
        });
    }


    /**
     * Change page event
     */
    function pageChanged() {
      $state.go('mypage.evaluation.list', {
        page: vm.currentPage
      });
    }


    function showEvaluation(montlyIndex) {
      vm.isEvaluation = true;
      vm.montlyIndex = montlyIndex;

      if (vm.evaluations[vm.montlyIndex].course.productType === 'telephone') {
        vm.evaluations[vm.montlyIndex].course.productType = '전화';
      } else if (vm.evaluations[vm.montlyIndex].course.productType === 'skype') {
        vm.evaluations[vm.montlyIndex].course.productType = '스카이프';
      } else if (vm.evaluations[vm.montlyIndex].course.productType === 'screenBoard') {
        vm.evaluations[vm.montlyIndex].course.productType = '화상칠판';
      }
      vm.courseInfo = vm.evaluations[vm.montlyIndex].course.productType + ' 주 ' + vm.evaluations[vm.montlyIndex].course.payment.paymentedProduct.times + ' 회';

    }


  }

}());
