(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('PaymentController', PaymentController);

  /**
   * Dependency Injection
   */
  PaymentController.$inject = [
    '$window',
    '$state',
    '$scope',
    'Authentication',
    'PaymentService'
  ];

  /**
   * Configuring the MyPage Payment list controller
   */
  function PaymentController(
    $window,
    $state,
    $scope,
    Authentication,
    PaymentService
  ) {
    const vm = this;
    vm.auth = Authentication;

    vm.pageChanged = pageChanged;
    vm.openProof = openProof;
    vm.show = {};


    /**
     * Today's date for pop-up
     */
    function buildToday() {
      const today = new Date();
      let dd = today.getDate();
      let mm = today.getMonth() + 1;
      const yyyy = today.getFullYear();

      if (dd < 10) {
        dd = '0' + dd;
      } else if (mm < 10) {
        mm = '0' + mm;
      }

      vm.today = yyyy + '-' + mm + '-' + dd;
    }


    /**
     * Pop-up for receipts, attendance certificates, and course cards
     */
    function openProof(paymentIndex, titles) {
      vm.titles = titles;
      vm.paymentIdx = paymentIndex;
      buildToday();
    }


    /**
     * When you are not signIn
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    }

    /**
     * When there are no $state.params.page
     */
    if (!$state.params.page) {
      $state.go('mypage.payment', {
        page: 1
      });
    }

    /**
     * To import payment List data and create a list
     */
    PaymentService.paymentList($state.params.page)
      .then((payments) => {
        vm.payments = payments;
        // console.log(vm.payments);
        buildPager();
      })
      .catch((err) => {
        console.log(err);
      });

    /**
     * Build page
     */
    function buildPager() {
      PaymentService.paymentTotalCount()
        .$promise
        .then((totalCount) => {
          vm.totalItems = totalCount.counts;
          vm.currentPage = $state.params.page;
          vm.itemsPerPage = 5;
        })
        .catch((err) => {
          console.log(err);
        });
    }


    /**
     * Change page event
     */
    function pageChanged() {
      $state.go('mypage.payment', {
        page: vm.currentPage
      });
    }




  }

}());
