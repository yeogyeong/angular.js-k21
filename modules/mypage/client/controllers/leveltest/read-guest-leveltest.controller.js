(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('ReadGuestLevelController', ReadGuestLevelController);

  /**
   * Dependency Injection
   */
  ReadGuestLevelController.$inject = [
    '$window',
    '$state',
    'leveltestResolve',
    'MyPageLevelService',
    'ModalService'
  ];

  /**
   * Configuring the Guest Leveltest read controller
   */
  function ReadGuestLevelController(
    $window,
    $state,
    leveltestResolve,
    MyPageLevelService,
    ModalService
  ) {
    const vm = this;

  }
}());
