(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('ListMyPageLevelController', ListMyPageLevelController);

  /**
   * Dependency Injection
   */
  ListMyPageLevelController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'MyPageLevelService'
  ];

  /**
   * Configuring the MyPage Leveltest list controller
   */
  function ListMyPageLevelController(
    $window,
    $state,
    Authentication,
    MyPageLevelService
  ) {
    const vm = this;
    vm.auth = Authentication;
    vm.pageChanged = pageChanged;

    /**
     * When you are not signIn
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    }

    /**
     * When there are no $state.params.page
     */
    if (!$state.params.page) {
      $state.go('mypage.leveltest.list', {
        page: 1
      });
    }


    /**
     * To import Leveltest list data and create a list
     */
    MyPageLevelService.leveltestList($state.params.page)
      .then((levelResults) => {
        vm.levelResults = levelResults;
        buildPager();
      })
      .catch((err) => {
        console.log(err);
      });

    /**
     * Build page
     */
    function buildPager() {
      MyPageLevelService.leveltestTotalCount()
        .$promise
        .then((totalCount) => {
          vm.totalItems = totalCount.counts;
          vm.currentPage = $state.params.page;
          vm.itemsPerPage = 5;
        })
        .catch((err) => {
          console.log(err);
        });
    }


    /**
     * Change page event
     */
    function pageChanged() {
      $state.go('mypage.leveltest.list', {
        page: vm.currentPage
      });
    }

  }

}());
