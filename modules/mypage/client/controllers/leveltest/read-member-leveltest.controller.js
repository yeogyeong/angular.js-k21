(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('ReadMemberLevelController', ReadMemberLevelController);

  /**
   * Dependency Injection
   */
  ReadMemberLevelController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'leveltestResolve',
    'MyPageLevelService',
    'ModalService'
  ];

  /**
   * Configuring the MyPage Leveltest list controller
   */
  function ReadMemberLevelController(
    $window,
    $state,
    Authentication,
    leveltestResolve,
    MyPageLevelService,
    ModalService
  ) {
    const vm = this;
    vm.auth = Authentication;
    vm.leveltests = leveltestResolve;
    vm.assessment = vm.leveltests.assessment;
    // console.log(vm.leveltests);
    /**
     * Total leveltest
     */
    vm.totalLevel = totalLevel;

    /**
     * textarea enter replace
     */
    if (vm.assessment.tutorComment) {
      vm.assessment.tutorComment = vm.assessment.tutorComment.replace(/(?:\r\n|\r|\n)/g, '<br />');
    }


    /**
     * Chart label
     */
    vm.labels = ['Listening', 'Speaking', 'Pronunciation', 'Vocabulary', 'Grammar'];


    vm.datasets = [{
      backgroundColor: 'rgba(255,255,255, 0)',
      borderColor: 'rgba(132,132,212, 1)'
    }];

    vm.series = ['chartData'];


    /**
     * Chart opionts
     */
    vm.chartOptions = {
      scale: {
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 100,
          stepSize: 20
        }
      }
    };


    /**
     * Chart dataset
     */
    vm.chartData = [
      [
        vm.assessment.listeningLevel = vm.assessment.listeningLevel ? (vm.assessment.listeningLevel * 10) : 10,
        vm.assessment.speakingLevel = vm.assessment.speakingLevel ? (vm.assessment.speakingLevel * 10) : 10,
        vm.assessment.pronunciationLevel = vm.assessment.pronunciationLevel ? (vm.assessment.pronunciationLevel * 10) : 10,
        vm.assessment.vocabularyLevel = vm.assessment.vocabularyLevel ? (vm.assessment.vocabularyLevel * 10) : 10,
        vm.assessment.grammarLevel = vm.assessment.grammarLevel ? (vm.assessment.grammarLevel * 10) : 10
      ]
    ];

    /**
     * Total level leveltest
     */
    function totalLevel(score) {

      const averageLevels = [
        score.pronunciationLevel = score.pronunciationLevel ? score.pronunciationLevel : 1,
        score.grammarLevel = score.grammarLevel ? score.grammarLevel : 1,
        score.listeningLevel = score.listeningLevel ? score.listeningLevel : 1,
        score.speakingLevel = score.speakingLevel ? score.speakingLevel : 1,
        score.vocabularyLevel = score.vocabularyLevel ? score.vocabularyLevel : 1
      ];

      return Math.round((averageLevels.reduce((accumulator, currentValue) => {
        return accumulator + currentValue;
      }) / 50));
    }


    vm.recording = recording;

    function recording() {
         vm.isRecording = true;
    }
  }
}());
