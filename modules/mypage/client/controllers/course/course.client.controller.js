(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('CourseController', CourseController);

  /**
   * Dependency Injection
   */
  CourseController.$inject = [
    '$window',
    '$scope',
    '$state',
    'Authentication',
    'MypageCourseService'
  ];

  /**
   * Configuring the MyPage Leveltest list controller
   */
  function CourseController(
    $window,
    $scope,
    $state,
    AuthService,
    MypageCourseService
  ) {
    const vm = this;
    vm.auth = AuthService;
    vm.selectCourse = selectCourse;

    /**
     * When you are not signIn
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    }


    /**
     * when the month and year change
     */
    $scope.$watch('vm.months', (months) => {
      if (months) {
        vm.year = moment(months).format('YYYY');
        vm.month = moment(months).format('MM');
        getSchedule(vm.courseId, vm.year, vm.month);
      }
    });


    MypageCourseService.query((courses) => {
      vm.courses = courses;
      console.log(vm.courses);
      /**
       * Change to Korean on the productType
       */
      courses.forEach((courses) => {

        const productType = [{
          type: 'telephone',
          name: '전화'
        }, {
          type: 'skype',
          name: '스카이프'
        }, {
          type: 'screenBoard',
          name: '화상칠판'
        }];

        productType.forEach((productTypes) => {
          if (courses.virtualProduct.productType === productTypes.type) {
            courses.productType = productTypes.name;
          }
        });
        vm.courseWay = courses.productType;
      });

      /**
       * Set selectbox value
       */
      if (courses) {
        vm.courseTitle = '0';
      }

      selectCourse();
    });



    /**
     * SelectBox is Pressed
     */
    function selectCourse() {
      const isCourses = vm.courses[0];

      if (isCourses) {
        const courseInfo = vm.courses[vm.courseTitle];

        /**
         * Change to Korean on the selected day of the week
         */
        const days = courseInfo.daysOfWeek;
        vm.selectDays = [];
        days.forEach((days) => {
          const isSelectedDay = days.selected;
          const koreanDays = ['월', '화', '수', '목', '금'];

          if (isSelectedDay) {
            let daysName = days.name;

            daysName = koreanDays[days.index - 1];
            vm.selectDays.push(daysName);
          }
        });

        /**
         * Change to Korean on the courseInfo
         */
        if (courseInfo.state === 'scheduled') {
          courseInfo.state = '신청완료';
        } else if (courseInfo.state === 'inProgress') {
          courseInfo.state = '수강중';
        } else if (courseInfo.state === 'hold') {
          courseInfo.state = '연기';
        } else if (courseInfo.state === 'completed') {
          courseInfo.state = '수강완료';
        }



        vm.courseInfo = courseInfo.payment.paymentedProduct.month + '개월 주' + courseInfo.payment.paymentedProduct.times + '회 ' + courseInfo.payment.paymentedProduct.minutes + '분';
        vm.courseWay = courseInfo.productType;
        vm.courseState = courseInfo.state;


        /**
         * courseId,year,month required for the calendar
         */
        vm.courseId = courseInfo._id;
        vm.year = moment(courseInfo.startLesson.started).format('YYYY');
        vm.month = moment(courseInfo.startLesson.started).format('MM');
        getSchedule(vm.courseId, vm.year, vm.month);
      }

    }


    /**
     * Get ScheduleData for the courseId,month,year
     */
    function getSchedule(courseId, year, month) {
      MypageCourseService.calendarList(courseId, year, month)
        .then((scheduled) => {
          vm.scheduled = scheduled;
        })
        .catch((err) => {
          console.log(err);
        });
    }


  }

}());
