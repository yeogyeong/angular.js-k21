(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('PointController', PointController);

  /**
   * Dependency Injection
   */
  PointController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'PointService'
  ];

  /**
   * Configuring the MyPage Point list controller
   */
  function PointController(
    $window,
    $state,
    Authentication,
    PointService
  ) {
    const vm = this;
    vm.auth = Authentication;
    vm.pageChanged = pageChanged;

    /**
     * When you are not signIn
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    }


    /**
     * When there are no $state.params.page
     */
    if (!$state.params.page) {
      $state.go('mypage.point', {
        page: 1
      });
    }

    /**
     * To import point List data and create a list
     */
    PointService.pointList($state.params.page)
      .then((points) => {
        vm.points = points;
        buildPager();
      })
      .catch((err) => {
        console.log(err);
      });

    /**
     * Build page
     */
    function buildPager() {
      PointService.pointTotalCount()
        .$promise
        .then((totalCount) => {
          vm.totalItems = totalCount.counts;
          vm.currentPage = $state.params.page;
          vm.itemsPerPage = 5;
        })
        .catch((err) => {
          console.log(err);
        });
    }


    /**
     * Change page event
     */
    function pageChanged() {
      $state.go('mypage.point', {
        page: vm.currentPage
      });
    }




  }

}());
