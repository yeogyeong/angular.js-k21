(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('CouponController', CouponController);

  /**
   * Dependency Injection
   */
  CouponController.$inject = [
    '$window',
    '$state',
    '$scope',
    'Authentication',
    'CouponService'
  ];

  /**
   * Configuring the MyPage Coupon list controller
   */
  function CouponController(
    $window,
    $state,
    $scope,
    Authentication,
    CouponService
  ) {
    const vm = this;
    vm.auth = Authentication;

    vm.confirm = confirm;
    vm.cleanCouponText = cleanCouponText;

    vm.pageChanged = pageChanged;
    vm.couponCode = [];


    /**
     * When you are not signIn
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    }

    /**
     * When there are no $state.params.page
     */
    if (!$state.params.page) {
      $state.go('mypage.coupon', {
        page: 1
      });
    }

    /**
     * To import coupon List data and create a list
     */
    CouponService.couponList($state.params.page)
      .then((coupons) => {
        vm.coupons = coupons;
        buildPager();
      })
      .catch((err) => {
        console.log(err);
      });

    /**
     * Build page
     */
    function buildPager() {
      CouponService.couponTotalCount()
        .$promise
        .then((totalCount) => {
          vm.vaildCount = totalCount.vaildCount;
          vm.totalItems = totalCount.totalCount;
          vm.currentPage = $state.params.page;
          vm.itemsPerPage = 5;
        })
        .catch((err) => {
          console.log(err);
        });
    }


    /**
     * Change page event
     */
    function pageChanged() {
      $state.go('mypage.coupon', {
        page: vm.currentPage
      });
    }



    /**
     * Press confirm button after Enter coupon number
     */
    function confirm() {
      vm.code = vm.couponCode.join('');
      vm.registed = new Date();
      if (!vm.couponCode || vm.code.length < 16) {
        $window.alert('쿠폰 번호 16자리를 입력해주세요.');
        return false;
      }

      CouponService.createCoupon({
          code: vm.code,
          registed: vm.registed
        })
        .then((res) => {
          console.log(res);
          vm.message = '쿠폰 등록이 완료되었습니다.';
          vm.isSuccess = true;
        })
        .catch((err) => {
          if (err.status === 422) {
            if (err.data.error === 'already used to coupon') {
              vm.message = '이미 등록된 쿠폰입니다.';
              vm.isSuccess = false;
            } else if (err.data.error === 'coupon serial number is not exits') {
              vm.message = '일치하는 쿠폰을 찾을 수 없습니다.';
              vm.isSuccess = false;
            }
          }
        });
    }

    /**
     * Press coupon close button
     */
    function cleanCouponText() {
      vm.isRegister = false;
      vm.message = '';
      vm.couponCode = [];
    }


  }

}());
