(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('ListMyPageConsultController', ListMyPageConsultController);

  /**
   * Dependency Injection
   */
  ListMyPageConsultController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'MyPageConsultService'
  ];

  /**
   * Configuring the MyPage Consult list controller
   */
  function ListMyPageConsultController(
    $window,
    $state,
    Authentication,
    MyPageConsultService
  ) {
    const vm = this;
    vm.auth = Authentication;
    vm.pageChanged = pageChanged;

    vm.searching = searching;
    vm.search = {};

    /**
     * When you are not signIn
     */
    if (!vm.auth.user) {
      $window.alert('로그인이 필요한 서비스 입니다.');
      $state.go('user.signin');
      return false;
    }

    /**
     * When there are no $state.params.page
     */
    if (!$state.params.page) {
      $state.go('mypage.consult.list', {
        page: 1
      });
    }


    /**
     * To import Leveltest list data and create a list
     */
    MyPageConsultService.consultList(
        $state.params.page,
        $state.params.searchOptions,
        $state.params.searchKeyword
      )
      .then((consults) => {
        vm.consults = consults;
        buildPager();
      })
      .catch((err) => {
        console.log(err);
      });

      /*
       * When you press the searching button
       * Using Parameters
       */
      function searching() {
        vm.currentPage = 1;
        vm.search.searchOptions = vm.searchOptions;

        if (!vm.searchKeyword) {
          vm.search.searchOptions = '';
        }

        $state.go('mypage.consult.list', {
          page: vm.currentPage,
          searchOptions: vm.search.searchOptions,
          searchKeyword: vm.searchKeyword
        });
        buildPager();

      }


    /**
     * Import total number of data to create page count
     */
    function buildPager() {
      vm.search.searchOptions = $state.params.searchOptions;
      vm.search.searchKeyword = $state.params.searchKeyword;
      MyPageConsultService.consultTotalCount(vm.search)
        .$promise
        .then((totalCount) => {
          vm.totalItems = totalCount.counts;
          vm.currentPage = $state.params.page;
          vm.itemsPerPage = 5;

          if (!$state.params.searchOptions) {
            vm.searchOptions = 'title';
          } else {
            vm.searchOptions = $state.params.searchOptions;
            vm.searchKeyword = $state.params.searchKeyword;
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }



    /**
     * Change page event
     */
    function pageChanged() {
      $state.go('mypage.consult.list', {
        page: vm.currentPage
      });
    }


  }

}());
