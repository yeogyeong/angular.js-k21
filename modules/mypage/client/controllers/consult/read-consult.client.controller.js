(function() {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .controller('ReadMyPageConsultController', ReadMyPageConsultController);

  /**
   * Dependency Injection
   */
  ReadMyPageConsultController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'consultResolve'
  ];

  /**
   * Read Course Review controller
   */
  function ReadMyPageConsultController(
    $window,
    $state,
    Authentication,
    consultResolve
  ) {

    const vm = this;
    vm.auth = Authentication;
    vm.consult = consultResolve;

    vm.remove = remove;


    /**
     * textarea enter replace
     */
    vm.consult.contents = vm.consult.contents.replace(/(?:\r\n|\r|\n)/g, '<br />');
    // if (vm.consult.comments[0]) {
    //   vm.consult.comments[0].contents = vm.consult.comments[0].contents.replace(/(?:\r\n|\r|\n)/g, '<br />');
    // }

    /**
     * remove consult
     */
    function remove() {
      vm.consult.$remove()
        .then((response) => {
          $window.alert('글이 삭제되었습니다');
          $state.go('mypage.consult.list');
        })
        .catch((err) => {
          console.log(err);
        });
    }

  }

}());
