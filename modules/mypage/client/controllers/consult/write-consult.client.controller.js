(function() {

  /**
   * Module Configuration
   */
  angular
    .module('mypage.routes')
    .controller('WriteConsultController', WriteConsultController);

  /**
   * Dependency Injection
   */
  WriteConsultController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'consultResolve',
    'MyPageConsultService'
  ];

  /**
   * Configuring the MyPage Write Consult controller
   */
  function WriteConsultController(
    $window,
    $state,
    Authentication,
    consult,
    MyPageConsultService
  ) {
    const vm = this;
    vm.auth = Authentication;
    vm.registration = registration;
    vm.consult = consult;

    MyPageConsultService.mainTutor((tutors) => {
      vm.tutors = tutors;
    });


    function registration() {
      vm.consult.user = vm.auth.user._id;

      if (!vm.consult.title) {
        $window.alert('제목을 입력해주세요');
        return false;
      } else if (!vm.consult.tutor) {
        $window.alert('담당강사를 선택해주세요');
        return false;
      } else if (!vm.consult.contents) {
        $window.alert('내용을 입력해주세요');
        return false;
      }
      console.log(vm.consult);

      vm.consult.$save()
        .then((res) => {
          $window.alert('문의글이 등록되었습니다.');
          $state.go('mypage.consult.list');
        })
        .catch((err) => {
          console.log(err);
        });
    }


  }

}());
