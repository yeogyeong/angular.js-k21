(function() {

  /**
   * Module Configuration
   */
  angular
    .module('core')
    .controller('EditMyPageConsultController', EditMyPageConsultController);

  /**
   * Dependency Injection
   */
  EditMyPageConsultController.$inject = [
    '$window',
    '$state',
    'Authentication',
    'consultResolve'
  ];

  /**
   * Course Review edit controller
   */
  function EditMyPageConsultController(
    $window,
    $state,
    Authentication,
    consultResolve

  ) {

    const vm = this;

    vm.consult = consultResolve;

    vm.edit = edit;

/**
 * edit consult($update)
 */
    function edit(contact1x1Id) {
      vm.consult.$update()
        .then((response) => {
          $window.alert('담당강사문의가 수정되었습니다');
          $state.go('mypage.consult.read', {
            contact1x1Id
          });
        })
        .catch();
    }
  }

}());
