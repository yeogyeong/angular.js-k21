(function(app) {
  app.registerModule('mypage', ['core']);
  app.registerModule('mypage.services');
  app.registerModule('mypage.routes', ['core.routes', 'mypage.services']);
}(ApplicationConfiguration));
